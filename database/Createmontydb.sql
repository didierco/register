-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema montrealDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema montrealDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `montrealDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `montrealDB` ;

-- -----------------------------------------------------
-- Table `montrealDB`.`couleurs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`couleurs` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`couleurs` (
  `idcouleur` INT NOT NULL AUTO_INCREMENT,
  `codeCouleur` VARCHAR(45) NULL,
  PRIMARY KEY (`idcouleur`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`status` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`status` (
  `idstatus` INT NOT NULL AUTO_INCREMENT,
  `Code` VARCHAR(45) NULL,
  PRIMARY KEY (`idstatus`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`pays`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`pays` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`pays` (
  `idpays` INT NOT NULL AUTO_INCREMENT,
  `codePays` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`idpays`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`proprietaires`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`proprietaires` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`proprietaires` (
  `idproprietaire` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NULL,
  `prenom` VARCHAR(45) NULL,
  `pays_idpays` INT NOT NULL,
  `motDePasse` VARCHAR(255) NULL,
  `actif` TINYINT(1) NOT NULL DEFAULT 0,
  `adresseMail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idproprietaire`),
  CONSTRAINT `fk_proprietaires_pays1`
    FOREIGN KEY (`pays_idpays`)
    REFERENCES `montrealDB`.`pays` (`idpays`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_proprietaires_pays1_idx` ON `montrealDB`.`proprietaires` (`pays_idpays` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`historiques`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`historiques` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`historiques` (
  `idhistorique` INT NOT NULL AUTO_INCREMENT,
  `debut` DATE NULL,
  `fin` DATE NULL,
  `proprietaire_idproprietaire` INT NOT NULL,
  `Montreals_idMontreal` INT NOT NULL,
  `suivant_idhistorique` INT NULL,
  PRIMARY KEY (`idhistorique`),
  CONSTRAINT `fk_historique_proprietaire1`
    FOREIGN KEY (`proprietaire_idproprietaire`)
    REFERENCES `montrealDB`.`proprietaires` (`idproprietaire`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_historiques_Montreals1`
    FOREIGN KEY (`Montreals_idMontreal`)
    REFERENCES `montrealDB`.`Montreals` (`idMontreal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `suivant`
    FOREIGN KEY (`suivant_idhistorique`)
    REFERENCES `montrealDB`.`historiques` (`idhistorique`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_historique_proprietaire1_idx` ON `montrealDB`.`historiques` (`proprietaire_idproprietaire` ASC);

CREATE INDEX `fk_historiques_Montreals1_idx` ON `montrealDB`.`historiques` (`Montreals_idMontreal` ASC);

CREATE INDEX `fk_historiques_historiques1_idx` ON `montrealDB`.`historiques` (`suivant_idhistorique` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`interieurs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`interieurs` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`interieurs` (
  `idinterieur` INT NOT NULL AUTO_INCREMENT,
  `codeInterieur` VARCHAR(5) NULL,
  PRIMARY KEY (`idinterieur`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`noSerie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`noSerie` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`noSerie` (
  `idnoSerie` INT NOT NULL AUTO_INCREMENT,
  `annee` TINYINT NOT NULL,
  `drive` CHAR(1) NOT NULL,
  `noSerie` SMALLINT NOT NULL,
  PRIMARY KEY (`idnoSerie`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`Montreals`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`Montreals` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`Montreals` (
  `idMontreal` INT NOT NULL AUTO_INCREMENT,
  `anneeDeFabrication` DATE NULL,
  `photo` VARCHAR(256) NULL,
  `couleur_idcouleur` INT NOT NULL,
  `status_idstatus` INT NOT NULL,
  `historiques_idhistorique` INT NOT NULL,
  `interieurs_idinterieur` INT NOT NULL,
  `noSerie_idnoSerie` INT NOT NULL,
  PRIMARY KEY (`idMontreal`),
  CONSTRAINT `fk_Montreal_couleur1`
    FOREIGN KEY (`couleur_idcouleur`)
    REFERENCES `montrealDB`.`couleurs` (`idcouleur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Montreals_status1`
    FOREIGN KEY (`status_idstatus`)
    REFERENCES `montrealDB`.`status` (`idstatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Montreals_historiques1`
    FOREIGN KEY (`historiques_idhistorique`)
    REFERENCES `montrealDB`.`historiques` (`idhistorique`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Montreals_interieurs1`
    FOREIGN KEY (`interieurs_idinterieur`)
    REFERENCES `montrealDB`.`interieurs` (`idinterieur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Montreals_noSerie1`
    FOREIGN KEY (`noSerie_idnoSerie`)
    REFERENCES `montrealDB`.`noSerie` (`idnoSerie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Montreal_couleur1_idx` ON `montrealDB`.`Montreals` (`couleur_idcouleur` ASC);

CREATE INDEX `fk_Montreals_status1_idx` ON `montrealDB`.`Montreals` (`status_idstatus` ASC);

CREATE INDEX `fk_Montreals_historiques1_idx` ON `montrealDB`.`Montreals` (`historiques_idhistorique` ASC);

CREATE INDEX `fk_Montreals_interieurs1_idx` ON `montrealDB`.`Montreals` (`interieurs_idinterieur` ASC);

CREATE INDEX `fk_Montreals_noSerie1_idx` ON `montrealDB`.`Montreals` (`noSerie_idnoSerie` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`faq`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`faq` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`faq` (
  `idfaq` INT NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(2) NULL,
  `texte` TEXT NULL,
  `date_creation` DATE NULL,
  `auteur` VARCHAR(45) NULL,
  `titre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idfaq`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`news`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`news` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`news` (
  `idnews` INT NOT NULL,
  `titre` VARCHAR(45) NOT NULL,
  `texte` TEXT NULL,
  `dateCreation` DATE NULL,
  PRIMARY KEY (`idnews`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `montrealDB`.`Attente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`Attente` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`Attente` (
  `idAttente` INT NOT NULL AUTO_INCREMENT,
  `anneeDeFabrication` TINYINT(1) NULL,
  `photo` VARCHAR(256) NULL,
  `noSerie` SMALLINT NULL,
  `pays_idpays` INT NOT NULL,
  `interieurs_idinterieur` INT NOT NULL,
  `couleurs_idcouleur` INT NOT NULL,
  `status_idstatus` INT NOT NULL,
  `descriptionCouleur` VARCHAR(45) NULL,
  `descriptionInterieur` VARCHAR(45) NULL,
  `proprietaires_idproprietaire` INT NOT NULL,
  PRIMARY KEY (`idAttente`),
  CONSTRAINT `fk_Attente_pays1`
    FOREIGN KEY (`pays_idpays`)
    REFERENCES `montrealDB`.`pays` (`idpays`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Attente_interieurs1`
    FOREIGN KEY (`interieurs_idinterieur`)
    REFERENCES `montrealDB`.`interieurs` (`idinterieur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Attente_couleurs1`
    FOREIGN KEY (`couleurs_idcouleur`)
    REFERENCES `montrealDB`.`couleurs` (`idcouleur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Attente_status1`
    FOREIGN KEY (`status_idstatus`)
    REFERENCES `montrealDB`.`status` (`idstatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Attente_proprietaires1`
    FOREIGN KEY (`proprietaires_idproprietaire`)
    REFERENCES `montrealDB`.`proprietaires` (`idproprietaire`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Attente_pays1_idx` ON `montrealDB`.`Attente` (`pays_idpays` ASC);

CREATE INDEX `fk_Attente_interieurs1_idx` ON `montrealDB`.`Attente` (`interieurs_idinterieur` ASC);

CREATE INDEX `fk_Attente_couleurs1_idx` ON `montrealDB`.`Attente` (`couleurs_idcouleur` ASC);

CREATE INDEX `fk_Attente_status1_idx` ON `montrealDB`.`Attente` (`status_idstatus` ASC);

CREATE INDEX `fk_Attente_proprietaires1_idx` ON `montrealDB`.`Attente` (`proprietaires_idproprietaire` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`localeCouleur`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`localeCouleur` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`localeCouleur` (
  `idlocaleCouleur` INT NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(2) NOT NULL,
  `description` VARCHAR(256) NULL,
  `couleurs_idcouleur` INT NOT NULL,
  `codeCouleur` VARCHAR(10) NULL,
  PRIMARY KEY (`idlocaleCouleur`),
  CONSTRAINT `fk_localeCouleur_couleurs1`
    FOREIGN KEY (`couleurs_idcouleur`)
    REFERENCES `montrealDB`.`couleurs` (`idcouleur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_localeCouleur_couleurs1_idx` ON `montrealDB`.`localeCouleur` (`couleurs_idcouleur` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`localeInterieur`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`localeInterieur` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`localeInterieur` (
  `idlocaleInterieur` INT NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(2) NOT NULL,
  `description` VARCHAR(256) NULL,
  `interieurs_idinterieur` INT NOT NULL,
  PRIMARY KEY (`idlocaleInterieur`),
  CONSTRAINT `fk_localeInterieur_interieurs1`
    FOREIGN KEY (`interieurs_idinterieur`)
    REFERENCES `montrealDB`.`interieurs` (`idinterieur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_localeInterieur_interieurs1_idx` ON `montrealDB`.`localeInterieur` (`interieurs_idinterieur` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`localeStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`localeStatus` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`localeStatus` (
  `idlocaleStatus` INT NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(2) NOT NULL,
  `description` VARCHAR(256) NULL,
  `status_idstatus` INT NOT NULL,
  PRIMARY KEY (`idlocaleStatus`),
  CONSTRAINT `fk_localeStatus_status1`
    FOREIGN KEY (`status_idstatus`)
    REFERENCES `montrealDB`.`status` (`idstatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_localeStatus_status1_idx` ON `montrealDB`.`localeStatus` (`status_idstatus` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`localePays`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`localePays` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`localePays` (
  `idlocalePays` INT NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(2) NOT NULL,
  `description` VARCHAR(256) NULL,
  `pays_idpays` INT NOT NULL,
  PRIMARY KEY (`idlocalePays`),
  CONSTRAINT `fk_localePays_pays1`
    FOREIGN KEY (`pays_idpays`)
    REFERENCES `montrealDB`.`pays` (`idpays`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_localePays_pays1_idx` ON `montrealDB`.`localePays` (`pays_idpays` ASC);


-- -----------------------------------------------------
-- Table `montrealDB`.`temppays`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `montrealDB`.`temppays` ;

CREATE TABLE IF NOT EXISTS `montrealDB`.`temppays` (
  `idpays` INT NOT NULL AUTO_INCREMENT,
  `codePays` VARCHAR(5) NOT NULL,
  `pays` VARCHAR(45) NULL,
  `locale` VARCHAR(2) NULL,
  PRIMARY KEY (`idpays`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
