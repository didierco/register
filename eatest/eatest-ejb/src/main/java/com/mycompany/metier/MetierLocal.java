/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metier;

import com.mycompany.dao.Faq;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author Didier
 */
@Local
public interface MetierLocal {

    void create(Faq faq);

    void edit(Faq faq);

    void remove(Faq faq);

    Faq find(Object id);

    List<Faq> findAll();

    List<Faq> findRange(int[] range);

    boolean isInterieurExist(int couleur);  //est ce que la couleur existe dans la base?
    boolean isCouleurExist(int interieur);    // est ce que l'interieur existe en base?
    int count();
    
}
