package com.mycompany.metier;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import org.eclipse.persistence.expressions.ExpressionOperator;
 @Stateless
public class Erreurs {
private List<Erreur> erreurs ;
private boolean uneErreur;   // flag set si il y ades erreurs
  // constructeur
  public Erreurs(){
  erreurs = new ArrayList<>();
  uneErreur = false;
  }
  private void sendLesErreur(){
      for (Erreur erreur : erreurs) {
          FacesMessage erreurMsg = null;
          
          switch (erreur.getClasse()){
              case "W":
                   erreurMsg =   new FacesMessage(FacesMessage.SEVERITY_WARN,"", erreur.getMessage());
                  break;
              case "E":
                     erreurMsg =   new FacesMessage(FacesMessage.SEVERITY_ERROR,"", erreur.getMessage());
                  break;
                  
          }
                        
            FacesContext.getCurrentInstance().addMessage(null, erreurMsg);
      }
  
  }
  public void add(Erreur err){
      setUneErreur();
          erreurs.add(err);
  }
  public void reset(){
      uneErreur = false;
          erreurs.clear();
  }

    public List<Erreur> getErreurs() {
        return erreurs;
    }

    public void setErreurs(List<Erreur> erreurs) {
        this.erreurs = erreurs;
    }

    public boolean isUneErreur() {
        return uneErreur;
    }

    public void setUneErreur() {
        this.uneErreur = true;
    }

   
    public void sendErreurs(Locale locale) {
        System.out.println("erreur locale:"+locale.toString());
    
        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        for (Erreur erreur : erreurs) {

            try {
                System.out.println("SendErreur:"+erreur.getMessage());
                erreur.setMessage(messages.getString("erreur." + erreur.getMessage()));
            } catch (Exception e) {
                System.out.println("erreur locale" + e.getLocalizedMessage());
                throw (MissingResourceException) e;

            }
        }

        sendLesErreur();  // envoie toutes les erreurs 
    }

   


}

