
package com.mycompany.metier;
import com.mycompany.jpa.IMontrealLocal;
import com.mycompany.jpa.ISerieLocal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless

public class Metier {
    private List<Erreur> erreurs = new ArrayList<Erreur>();
    private Boolean erreur = false;
   
    @EJB
    private ISerieLocal serieDao;
    @EJB
    private IMontrealLocal montrealDao;
    public Metier() {
  }
   public void setErreur(Boolean erreur) {
        this.erreur = erreur;
    }
   public Boolean getErreur() {
        return erreur;
    }
    /*
     La voiture est elle référencée dans la base des numéro de série?
 
     */

    public Boolean isSerieExist(int noSerie, int annee, char drive) {

        return serieDao.isExist(noSerie, annee, drive);
    }
    /*
     La voiture est t'elle déja enregistrée?
     */

    public Boolean isExist(int noSerie) {

        return montrealDao.isExist(noSerie);
    }

    public List<Erreur> getErreurs() {
        return erreurs;
    }

}
