/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa;

import com.mycompany.dao.Noserie;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface ISerieLocal {

    void create(Noserie noSerie);

    void edit(Noserie noSerie);

    void remove(Noserie noSerie);

    Noserie find(Object id);

    List<Noserie> findAll();

    List<Noserie> findRange(int[] range);
    
public Boolean isExist(int noSerie,int annee, char drive);
    int count();

    
    
}
