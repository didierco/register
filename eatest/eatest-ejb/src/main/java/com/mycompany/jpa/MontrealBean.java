/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa;

import com.mycompany.dao.Montreals;
import com.mycompany.dao.Montreals;
import com.mycompany.dao.Montreals_;
import com.mycompany.dao.Noserie;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author Didier
 */
@Stateless
public class MontrealBean implements IMontrealLocal {
    @PersistenceContext(unitName = "com.mycompany_eatest-ejb_PU")
    private EntityManager em;

    

    public MontrealBean() {
       
    }

    @Override
    public void create(Montreals noSerie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(Montreals noSerie) {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Montreals noSerie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /*
    Renvoie un boolean pour checker si la voiture est deja dans la base
    */
  @Override
  public Boolean isExist(int noSerie) {
    
      
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        Root<Montreals> m = cq.from(Montreals.class);
        // jointure sur le numero de série
        Join<Montreals, Noserie> noserie = m.join(Montreals_.noSerieidnoSerie);

        cq.select(qb.count(m));
        cq.where(
                qb.equal(noserie.get("noSerie"), noSerie));
    //   cq.where(
        //   qb.equal(from.get("noSerieidnoSerie"), ns));
        Long result = em.createQuery(cq).getSingleResult();
      
          return result != 0;
    }

    @Override
    public Montreals find(Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Montreals> findRange(int[] range) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

    @Override
    public List<Montreals> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
