/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa;


import com.mycompany.dao.Montreals;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface IMontrealLocal {

    void create(Montreals noSerie);

    void edit(Montreals noSerie);

    void remove(Montreals noSerie);

    Montreals find(Object id);

    List<Montreals> findAll();

    List<Montreals> findRange(int[] range);
    
public Boolean isExist(int noSerie);


    
    
}
