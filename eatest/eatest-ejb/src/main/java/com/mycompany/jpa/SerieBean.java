/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa;

import com.mycompany.dao.Noserie;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Didier
 */
@Stateless
public class SerieBean implements ISerieLocal {
    @PersistenceContext(unitName = "com.mycompany_eatest-ejb_PU")
    private EntityManager em;

    

    public SerieBean() {
       
    }

    @Override
    public void create(Noserie noSerie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(Noserie noSerie) {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Noserie noSerie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /*
    Renvoie un boolean pour checker si le numero de série 
    existe dans la base de données des numéros de série 
    */
  @Override
  public Boolean isExist(int noSerie,int annee, char drive) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        Root<Noserie> from = cq.from(Noserie.class);
        cq.select(qb.count(from));
      /*  
        cq.where(
          qb.equal(from.get("noSerie"), noSerie),
                  qb.equal(from.get("drive"), drive),
                    qb.equal(from.get("annee"), annee));
        */
        /*
        on ne teste que le numero de série, la base de données des numéros
        tient compte des années de sortie de chaine et pas
        des années de mise en circumation
        */
      cq.where(
          qb.equal(from.get("noSerie"), noSerie));   
    Long result = em.createQuery(cq).getSingleResult();
    System.out.println("is serie:"+ result);
    
        return result != 0;
    }

    @Override
    public Noserie find(Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Noserie> findRange(int[] range) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Noserie> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
