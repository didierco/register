/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa;

import com.mycompany.dao.Montreals;
import com.mycompany.dao.Noserie;
import com.mycompany.exceptions.IllegalOrphanException;
import com.mycompany.exceptions.NonexistentEntityException;
import com.mycompany.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Didier
 */
public class NoserieJpaController implements Serializable {

    public NoserieJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
/*
    public void create(Noserie noserie) throws RollbackFailureException, Exception {
        if (noserie.getMontrealsList() == null) {
            noserie.setMontrealsList(new ArrayList<Montreals>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Montreals> attachedMontrealsList = new ArrayList<Montreals>();
            for (Montreals montrealsListMontrealsToAttach : noserie.getMontrealsList()) {
                montrealsListMontrealsToAttach = em.getReference(montrealsListMontrealsToAttach.getClass(), montrealsListMontrealsToAttach.getIdMontreal());
                attachedMontrealsList.add(montrealsListMontrealsToAttach);
            }
            noserie.setMontrealsList(attachedMontrealsList);
            em.persist(noserie);
            for (Montreals montrealsListMontreals : noserie.getMontrealsList()) {
         //       Noserie oldNoserieidnoSerieOfMontrealsListMontreals = montrealsListMontreals.getNoserieidnoSerie();
         //       montrealsListMontreals.setNoserieidnoSerie(noserie);
         //       montrealsListMontreals = em.merge(montrealsListMontreals);
          //      if (oldNoserieidnoSerieOfMontrealsListMontreals != null) {
              //      oldNoserieidnoSerieOfMontrealsListMontreals.getMontrealsList().remove(montrealsListMontreals);
            //        oldNoserieidnoSerieOfMontrealsListMontreals = em.merge(oldNoserieidnoSerieOfMontrealsListMontreals);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Noserie noserie) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Noserie persistentNoserie = em.find(Noserie.class, noserie.getIdnoSerie());
            List<Montreals> montrealsListOld = persistentNoserie.getMontrealsList();
            List<Montreals> montrealsListNew = noserie.getMontrealsList();
            List<String> illegalOrphanMessages = null;
            for (Montreals montrealsListOldMontreals : montrealsListOld) {
                if (!montrealsListNew.contains(montrealsListOldMontreals)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Montreals " + montrealsListOldMontreals + " since its noSerieidnoSerie field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Montreals> attachedMontrealsListNew = new ArrayList<Montreals>();
            for (Montreals montrealsListNewMontrealsToAttach : montrealsListNew) {
                montrealsListNewMontrealsToAttach = em.getReference(montrealsListNewMontrealsToAttach.getClass(), montrealsListNewMontrealsToAttach.getIdMontreal());
                attachedMontrealsListNew.add(montrealsListNewMontrealsToAttach);
            }
            montrealsListNew = attachedMontrealsListNew;
            noserie.setMontrealsList(montrealsListNew);
            noserie = em.merge(noserie);
            for (Montreals montrealsListNewMontreals : montrealsListNew) {
                if (!montrealsListOld.contains(montrealsListNewMontreals)) {
                    Noserie oldNoserieidnoSerieOfMontrealsListNewMontreals = montrealsListNewMontreals.getNoserieidnoSerie();
                    montrealsListNewMontreals.setNoserieidnoSerie(noserie);
                    montrealsListNewMontreals = em.merge(montrealsListNewMontreals);
                    if (oldNoserieidnoSerieOfMontrealsListNewMontreals != null && !oldNoserieidnoSerieOfMontrealsListNewMontreals.equals(noserie)) {
                        oldNoserieidnoSerieOfMontrealsListNewMontreals.getMontrealsList().remove(montrealsListNewMontreals);
                        oldNoserieidnoSerieOfMontrealsListNewMontreals = em.merge(oldNoserieidnoSerieOfMontrealsListNewMontreals);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = noserie.getIdnoSerie();
                if (findNoserie(id) == null) {
                    throw new NonexistentEntityException("The noserie with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Noserie noserie;
            try {
                noserie = em.getReference(Noserie.class, id);
                noserie.getIdnoSerie();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The noserie with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Montreals> montrealsListOrphanCheck = noserie.getMontrealsList();
            for (Montreals montrealsListOrphanCheckMontreals : montrealsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Noserie (" + noserie + ") cannot be destroyed since the Montreals " + montrealsListOrphanCheckMontreals + " in its montrealsList field has a non-nullable noSerieidnoSerie field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(noserie);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
*/
    public List<Noserie> findNoserieEntities() {
        return findNoserieEntities(true, -1, -1);
    }

    public List<Noserie> findNoserieEntities(int maxResults, int firstResult) {
        return findNoserieEntities(false, maxResults, firstResult);
    }

    private List<Noserie> findNoserieEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Noserie.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Noserie findNoserie(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Noserie.class, id);
        } finally {
            em.close();
        }
    }

    public int getNoserieCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Noserie> rt = cq.from(Noserie.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
