/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Attente;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Didier
 */
@Stateless
public class AttenteFacade extends AbstractFacade<Attente> implements AttenteFacadeLocal {
  //  public class AttenteFacade extends AbstractFacade<Attente> implements AttenteFacadeLocal {
    @PersistenceContext(unitName = "com.mycompany_eatest-ejb_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    @Override
 public Boolean create(Attente att) {
        try{
      //      getEntityManager().clear();
             boolean con = getEntityManager().contains(att.getProprietairesIdproprietaire());
            System.out.println("***********==== contains :"+con);
        getEntityManager().contains(att.getProprietairesIdproprietaire());
        getEntityManager().persist(att);
            //getEntityManager().flush();
           
        return true;}
        catch( Exception ex){
            System.out.println("** Erreur persistance sur l'entitée :" +att.getNoSerie()+ " " + ex.getMessage());
        return false;
        }
        
    }
    public AttenteFacade() {
        super(Attente.class);
    }
    
}
