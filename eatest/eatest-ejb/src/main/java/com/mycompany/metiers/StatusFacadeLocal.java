/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Status;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface StatusFacadeLocal {

    Boolean create(Status status);

    void edit(Status status);

    void remove(Status status);

    Status find(Object id);

    List<Status> findAll();

    List<Status> findRange(int[] range);

    int count();
    
}
