/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Proprietaires;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Didier
 */
@Stateless
public class ProprietairesFacade extends AbstractFacade<Proprietaires> implements ProprietairesFacadeLocal {
    @PersistenceContext(unitName = "com.mycompany_eatest-ejb_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
public void merge(Proprietaires proprio){
    em.merge(proprio);
}
    public ProprietairesFacade() {
        super(Proprietaires.class);
    }
  public Proprietaires findProprio(String adresse, String pwd) {
      Proprietaires result;
  
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        Root e = cq.from(Proprietaires.class);
        cq.select(e);
        cq.where(cb.and(cb.equal(e.get("adresseMail"),adresse),
                cb.equal(e.get("motDePasse"),pwd)));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
       try{
        result = (Proprietaires)q.getSingleResult();}
        
       catch( Exception ex){
            System.out.println("**select proprio not good :  " + ex.getMessage());
        return null;
      
    }
        System.out.println("**select proprio :  "+ result.getActif());
    return result;
  }

}
