/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Attente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface AttenteFacadeLocal {

    Boolean create(Attente attente);

    void edit(Attente attente);

    void remove(Attente attente);

    Attente find(Object id);

    List<Attente> findAll();

    List<Attente> findRange(int[] range);

    int count();
    
}
