/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Proprietaires;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local 
public interface ProprietairesFacadeLocal {

   Boolean create(Proprietaires proprietaires);
public Proprietaires findProprio(String adresse, String pwd);
    void edit(Proprietaires proprietaires);

    void remove(Proprietaires proprietaires);

    Proprietaires find(Object id);

    List<Proprietaires> findAll();

    List<Proprietaires> findRange(int[] range);

    int count();
    public void merge(Proprietaires proprio);
    
  
}
