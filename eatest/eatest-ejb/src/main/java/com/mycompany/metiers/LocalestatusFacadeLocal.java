/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.LocaleStatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface LocalestatusFacadeLocal {

    Boolean create(LocaleStatus localeStatus);

    void edit(LocaleStatus localeStatus);

    void remove(LocaleStatus localeStatus);

    LocaleStatus find(Object id);

    List<LocaleStatus> findAll();

    List<LocaleStatus> findRange(int[] range);

    int count();
    
}
