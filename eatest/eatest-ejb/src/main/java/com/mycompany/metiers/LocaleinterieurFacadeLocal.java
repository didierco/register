/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.LocaleInterieur;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface LocaleinterieurFacadeLocal {

    Boolean create(LocaleInterieur localeinterieur);

    void edit(LocaleInterieur localeinterieur);

    void remove(LocaleInterieur localeinterieur);

    LocaleInterieur find(Object id);

    List<LocaleInterieur> findAll();

    List<LocaleInterieur> findRange(int[] range);

    int count();
    
}
