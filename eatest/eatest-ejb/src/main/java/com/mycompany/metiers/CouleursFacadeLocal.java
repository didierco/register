/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Couleurs;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface CouleursFacadeLocal {

    Boolean create(Couleurs couleurs);

    void edit(Couleurs couleurs);

    void remove(Couleurs couleurs);

    Couleurs find(Object id);

    List<Couleurs> findAll();

    List<Couleurs> findRange(int[] range);

    int count();
    
}
