/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.LocalePays;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface LocalepaysFacadeLocal {

    Boolean create(LocalePays localePays);

    void edit(LocalePays localePays);

    void remove(LocalePays localePays);

    LocalePays find(Object id);

    List<LocalePays> findAll();

    List<LocalePays> findRange(int[] range);

    int count();
    
}
