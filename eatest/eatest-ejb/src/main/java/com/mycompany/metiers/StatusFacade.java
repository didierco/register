/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Status;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Didier
 */
@Stateless
public class StatusFacade extends AbstractFacade<Status> implements StatusFacadeLocal {
    @PersistenceContext(unitName = "com.mycompany_eatest-ejb_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StatusFacade() {
        super(Status.class);
    }

   

    
    
}
