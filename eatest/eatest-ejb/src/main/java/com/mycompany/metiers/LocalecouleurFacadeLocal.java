/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.LocaleCouleur;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface LocalecouleurFacadeLocal {

    Boolean create(LocaleCouleur localecouleur);

    void edit(LocaleCouleur localecouleur);

    void remove(LocaleCouleur localecouleur);

    LocaleCouleur find(Object id);

    List<LocaleCouleur> findAll();

    List<LocaleCouleur> findRange(int[] range);

    int count();
    
}
