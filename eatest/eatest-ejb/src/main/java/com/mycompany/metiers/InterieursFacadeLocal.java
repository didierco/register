/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.metiers;

import com.mycompany.dao.Interieurs;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Didier
 */
@Local
public interface InterieursFacadeLocal {

    Boolean create(Interieurs interieurs);

    void edit(Interieurs interieurs);

    void remove(Interieurs interieurs);

    Interieurs find(Object id);

    List<Interieurs> findAll();

    List<Interieurs> findRange(int[] range);

    int count();
    
}
