/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "montreals")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Montreals.findAll", query = "SELECT m FROM Montreals m"),
    @NamedQuery(name = "Montreals.findByIdMontreal", query = "SELECT m FROM Montreals m WHERE m.idMontreal = :idMontreal"),
    @NamedQuery(name = "Montreals.findByAnneeDeFabrication", query = "SELECT m FROM Montreals m WHERE m.anneeDeFabrication = :anneeDeFabrication"),
    @NamedQuery(name = "Montreals.findByPhoto", query = "SELECT m FROM Montreals m WHERE m.photo = :photo")})
public class Montreals implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMontreal")
    private Integer idMontreal;
    @Column(name = "anneeDeFabrication")
    @Temporal(TemporalType.DATE)
    private Date anneeDeFabrication;
    @Size(max = 256)
    @Column(name = "photo")
    private String photo;
    @JoinColumn(name = "status_idstatus", referencedColumnName = "idstatus")
    @ManyToOne(optional = false)
    private Status statusIdstatus;
    @JoinColumn(name = "noSerie_idnoSerie", referencedColumnName = "idnoSerie")
    @ManyToOne(optional = false)
    private Noserie noSerieidnoSerie;
    @JoinColumn(name = "interieurs_idinterieur", referencedColumnName = "idinterieur")
    @ManyToOne(optional = false)
    private Interieurs interieursIdinterieur;
    @JoinColumn(name = "historiques_idhistorique", referencedColumnName = "idhistorique")
    @ManyToOne(optional = false)
    private Historiques historiquesIdhistorique;
    @JoinColumn(name = "couleur_idcouleur", referencedColumnName = "idcouleur")
    @ManyToOne(optional = false)
    private Couleurs couleurIdcouleur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "montrealsidMontreal")
    private List<Historiques> historiquesList;

    public Montreals() {
    }

    public Montreals(Integer idMontreal) {
        this.idMontreal = idMontreal;
    }

    public Integer getIdMontreal() {
        return idMontreal;
    }

    public void setIdMontreal(Integer idMontreal) {
        this.idMontreal = idMontreal;
    }

    public Date getAnneeDeFabrication() {
        return anneeDeFabrication;
    }

    public void setAnneeDeFabrication(Date anneeDeFabrication) {
        this.anneeDeFabrication = anneeDeFabrication;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Status getStatusIdstatus() {
        return statusIdstatus;
    }

    public void setStatusIdstatus(Status statusIdstatus) {
        this.statusIdstatus = statusIdstatus;
    }

    public Noserie getNoSerieidnoSerie() {
        return noSerieidnoSerie;
    }

    public void setNoSerieidnoSerie(Noserie noSerieidnoSerie) {
        this.noSerieidnoSerie = noSerieidnoSerie;
    }

    public Interieurs getInterieursIdinterieur() {
        return interieursIdinterieur;
    }

    public void setInterieursIdinterieur(Interieurs interieursIdinterieur) {
        this.interieursIdinterieur = interieursIdinterieur;
    }

    public Historiques getHistoriquesIdhistorique() {
        return historiquesIdhistorique;
    }

    public void setHistoriquesIdhistorique(Historiques historiquesIdhistorique) {
        this.historiquesIdhistorique = historiquesIdhistorique;
    }

    public Couleurs getCouleurIdcouleur() {
        return couleurIdcouleur;
    }

    public void setCouleurIdcouleur(Couleurs couleurIdcouleur) {
        this.couleurIdcouleur = couleurIdcouleur;
    }

    @XmlTransient
    public List<Historiques> getHistoriquesList() {
        return historiquesList;
    }

    public void setHistoriquesList(List<Historiques> historiquesList) {
        this.historiquesList = historiquesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMontreal != null ? idMontreal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Montreals)) {
            return false;
        }
        Montreals other = (Montreals) object;
        if ((this.idMontreal == null && other.idMontreal != null) || (this.idMontreal != null && !this.idMontreal.equals(other.idMontreal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Montreals[ idMontreal=" + idMontreal + " ]";
    }
    
}
