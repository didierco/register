/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "faq")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Faq.findAll", query = "SELECT f FROM Faq f"),
    @NamedQuery(name = "Faq.findByIdfaq", query = "SELECT f FROM Faq f WHERE f.idfaq = :idfaq"),
    @NamedQuery(name = "Faq.findByLocale", query = "SELECT f FROM Faq f WHERE f.locale = :locale"),
    @NamedQuery(name = "Faq.findByDateCreation", query = "SELECT f FROM Faq f WHERE f.dateCreation = :dateCreation"),
    @NamedQuery(name = "Faq.findByAuteur", query = "SELECT f FROM Faq f WHERE f.auteur = :auteur"),
    @NamedQuery(name = "Faq.findByTitre", query = "SELECT f FROM Faq f WHERE f.titre = :titre")})
public class Faq implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfaq")
    private Integer idfaq;
    @Size(max = 2)
    @Column(name = "locale")
    private String locale;
    @Lob
    @Size(max = 65535)
    @Column(name = "texte")
    private String texte;
    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    @Size(max = 45)
    @Column(name = "auteur")
    private String auteur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "titre")
    private String titre;

    public Faq() {
    }

    public Faq(Integer idfaq) {
        this.idfaq = idfaq;
    }

    public Faq(Integer idfaq, String titre) {
        this.idfaq = idfaq;
        this.titre = titre;
    }

    public Integer getIdfaq() {
        return idfaq;
    }

    public void setIdfaq(Integer idfaq) {
        this.idfaq = idfaq;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfaq != null ? idfaq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Faq)) {
            return false;
        }
        Faq other = (Faq) object;
        if ((this.idfaq == null && other.idfaq != null) || (this.idfaq != null && !this.idfaq.equals(other.idfaq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Faq[ idfaq=" + idfaq + " ]";
    }
    
}
