/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "noserie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Noserie.findAll", query = "SELECT n FROM Noserie n"),
    @NamedQuery(name = "Noserie.findByIdnoSerie", query = "SELECT n FROM Noserie n WHERE n.idnoSerie = :idnoSerie"),
    @NamedQuery(name = "Noserie.findByAnnee", query = "SELECT n FROM Noserie n WHERE n.annee = :annee"),
    @NamedQuery(name = "Noserie.findByDrive", query = "SELECT n FROM Noserie n WHERE n.drive = :drive"),
    @NamedQuery(name = "Noserie.findByNoSerie", query = "SELECT n FROM Noserie n WHERE n.noSerie = :noSerie")})
public class Noserie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idnoSerie")
    private Integer idnoSerie;
    @Basic(optional = false)
    @NotNull
    @Column(name = "annee")
    private short annee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "drive")
    private Character drive;
    @Basic(optional = false)
    @NotNull
    @Column(name = "noSerie")
    private short noSerie;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "noSerieidnoSerie")
    private List<Montreals> montrealsList;

    public Noserie() {
    }

    public Noserie(Integer idnoSerie) {
        this.idnoSerie = idnoSerie;
    }

    public Noserie(Integer idnoSerie, short annee, Character drive, short noSerie) {
        this.idnoSerie = idnoSerie;
        this.annee = annee;
        this.drive = drive;
        this.noSerie = noSerie;
    }

    public Integer getIdnoSerie() {
        return idnoSerie;
    }

    public void setIdnoSerie(Integer idnoSerie) {
        this.idnoSerie = idnoSerie;
    }

    public short getAnnee() {
        return annee;
    }

    public void setAnnee(short annee) {
        this.annee = annee;
    }

    public Character getDrive() {
        return drive;
    }

    public void setDrive(Character drive) {
        this.drive = drive;
    }

    public short getNoSerie() {
        return noSerie;
    }

    public void setNoSerie(short noSerie) {
        this.noSerie = noSerie;
    }

    @XmlTransient
    public List<Montreals> getMontrealsList() {
        return montrealsList;
    }

    public void setMontrealsList(List<Montreals> montrealsList) {
        this.montrealsList = montrealsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idnoSerie != null ? idnoSerie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Noserie)) {
            return false;
        }
        Noserie other = (Noserie) object;
        if ((this.idnoSerie == null && other.idnoSerie != null) || (this.idnoSerie != null && !this.idnoSerie.equals(other.idnoSerie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Noserie[ idnoSerie=" + idnoSerie + " ]";
    }
    
}
