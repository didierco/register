/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Status.findAll", query = "SELECT s FROM Status s"),
    @NamedQuery(name = "Status.findByIdstatus", query = "SELECT s FROM Status s WHERE s.idstatus = :idstatus"),
    @NamedQuery(name = "Status.findByCode", query = "SELECT s FROM Status s WHERE s.code = :code")})
public class Status implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idstatus")
    private Integer idstatus;
    @Size(max = 45)
    @Column(name = "Code")
    private String code;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusIdstatus")
    private List<LocaleStatus> localestatusList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusIdstatus")
    private List<Montreals> montrealsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusIdstatus")
    private List<Attente> attenteList;

    public Status() {
    }

    public Status(Integer idstatus) {
        this.idstatus = idstatus;
    }

    public Integer getIdstatus() {
        return idstatus;
    }

    public void setIdstatus(Integer idstatus) {
        this.idstatus = idstatus;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public List<LocaleStatus> getLocalestatusList() {
        return localestatusList;
    }

    public void setLocalestatusList(List<LocaleStatus> localestatusList) {
        this.localestatusList = localestatusList;
    }

    @XmlTransient
    public List<Montreals> getMontrealsList() {
        return montrealsList;
    }

    public void setMontrealsList(List<Montreals> montrealsList) {
        this.montrealsList = montrealsList;
    }

    @XmlTransient
    public List<Attente> getAttenteList() {
        return attenteList;
    }

    public void setAttenteList(List<Attente> attenteList) {
        this.attenteList = attenteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idstatus != null ? idstatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Status)) {
            return false;
        }
        Status other = (Status) object;
        if ((this.idstatus == null && other.idstatus != null) || (this.idstatus != null && !this.idstatus.equals(other.idstatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Status[ idstatus=" + idstatus + " ]";
    }
    
}
