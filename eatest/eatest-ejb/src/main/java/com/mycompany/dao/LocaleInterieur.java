/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "localeinterieur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocaleInterieur.findAll", query = "SELECT l FROM LocaleInterieur l"),
    @NamedQuery(name = "LocaleInterieur.findByIdlocaleInterieur", query = "SELECT l FROM LocaleInterieur l WHERE l.idlocaleInterieur = :idlocaleInterieur"),
    @NamedQuery(name = "LocaleInterieur.findByLocale", query = "SELECT l FROM LocaleInterieur l WHERE l.locale = :locale"),
    @NamedQuery(name = "LocaleInterieur.findByDescription", query = "SELECT l FROM LocaleInterieur l WHERE l.description = :description")})
public class LocaleInterieur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlocaleInterieur")
    private Integer idlocaleInterieur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "locale")
    private String locale;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "interieurs_idinterieur", referencedColumnName = "idinterieur")
    @ManyToOne(optional = false)
    private Interieurs interieursIdinterieur;

    public LocaleInterieur() {
    }

    public LocaleInterieur(Integer idlocaleInterieur) {
        this.idlocaleInterieur = idlocaleInterieur;
    }

    public LocaleInterieur(Integer idlocaleInterieur, String locale) {
        this.idlocaleInterieur = idlocaleInterieur;
        this.locale = locale;
    }

    public Integer getIdlocaleInterieur() {
        return idlocaleInterieur;
    }

    public void setIdlocaleInterieur(Integer idlocaleInterieur) {
        this.idlocaleInterieur = idlocaleInterieur;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Interieurs getInterieursIdinterieur() {
        return interieursIdinterieur;
    }

    public void setInterieursIdinterieur(Interieurs interieursIdinterieur) {
        this.interieursIdinterieur = interieursIdinterieur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlocaleInterieur != null ? idlocaleInterieur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocaleInterieur)) {
            return false;
        }
        LocaleInterieur other = (LocaleInterieur) object;
        if ((this.idlocaleInterieur == null && other.idlocaleInterieur != null) || (this.idlocaleInterieur != null && !this.idlocaleInterieur.equals(other.idlocaleInterieur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.LocaleInterieur[ idlocaleInterieur=" + idlocaleInterieur + " ]";
    }
    
}
