/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "localepays")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalePays.findAll", query = "SELECT l FROM LocalePays l"),
    @NamedQuery(name = "LocalePays.findByIdlocalePays", query = "SELECT l FROM LocalePays l WHERE l.idlocalePays = :idlocalePays"),
    @NamedQuery(name = "LocalePays.findByLocale", query = "SELECT l FROM LocalePays l WHERE l.locale = :locale"),
    @NamedQuery(name = "LocalePays.findByDescription", query = "SELECT l FROM LocalePays l WHERE l.description = :description")})
public class LocalePays implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlocalePays")
    private Integer idlocalePays;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "locale")
    private String locale;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "pays_idpays", referencedColumnName = "idpays")
    @ManyToOne(optional = false)
    private Pays paysIdpays;

    public LocalePays() {
    }

    public LocalePays(Integer idlocalePays) {
        this.idlocalePays = idlocalePays;
    }

    public LocalePays(Integer idlocalePays, String locale) {
        this.idlocalePays = idlocalePays;
        this.locale = locale;
    }

    public Integer getIdlocalePays() {
        return idlocalePays;
    }

    public void setIdlocalePays(Integer idlocalePays) {
        this.idlocalePays = idlocalePays;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Pays getPaysIdpays() {
        return paysIdpays;
    }

    public void setPaysIdpays(Pays paysIdpays) {
        this.paysIdpays = paysIdpays;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlocalePays != null ? idlocalePays.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalePays)) {
            return false;
        }
        LocalePays other = (LocalePays) object;
        if ((this.idlocalePays == null && other.idlocalePays != null) || (this.idlocalePays != null && !this.idlocalePays.equals(other.idlocalePays))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.LocalePays[ idlocalePays=" + idlocalePays + " ]";
    }
    
}
