/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "historiques")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Historiques.findAll", query = "SELECT h FROM Historiques h"),
    @NamedQuery(name = "Historiques.findByIdhistorique", query = "SELECT h FROM Historiques h WHERE h.idhistorique = :idhistorique"),
    @NamedQuery(name = "Historiques.findByDebut", query = "SELECT h FROM Historiques h WHERE h.debut = :debut"),
    @NamedQuery(name = "Historiques.findByFin", query = "SELECT h FROM Historiques h WHERE h.fin = :fin")})
public class Historiques implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idhistorique")
    private Integer idhistorique;
    @Column(name = "debut")
    @Temporal(TemporalType.DATE)
    private Date debut;
    @Column(name = "fin")
    @Temporal(TemporalType.DATE)
    private Date fin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "historiquesIdhistorique")
    private List<Montreals> montrealsList;
    @OneToMany(mappedBy = "suivantIdhistorique")
    private List<Historiques> historiquesList;
    @JoinColumn(name = "suivant_idhistorique", referencedColumnName = "idhistorique")
    @ManyToOne
    private Historiques suivantIdhistorique;
    @JoinColumn(name = "Montreals_idMontreal", referencedColumnName = "idMontreal")
    @ManyToOne(optional = false)
    private Montreals montrealsidMontreal;
    @JoinColumn(name = "proprietaire_idproprietaire", referencedColumnName = "idproprietaire")
    @ManyToOne(optional = false)
    private Proprietaires proprietaireIdproprietaire;

    public Historiques() {
    }

    public Historiques(Integer idhistorique) {
        this.idhistorique = idhistorique;
    }

    public Integer getIdhistorique() {
        return idhistorique;
    }

    public void setIdhistorique(Integer idhistorique) {
        this.idhistorique = idhistorique;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    @XmlTransient
    public List<Montreals> getMontrealsList() {
        return montrealsList;
    }

    public void setMontrealsList(List<Montreals> montrealsList) {
        this.montrealsList = montrealsList;
    }

    @XmlTransient
    public List<Historiques> getHistoriquesList() {
        return historiquesList;
    }

    public void setHistoriquesList(List<Historiques> historiquesList) {
        this.historiquesList = historiquesList;
    }

    public Historiques getSuivantIdhistorique() {
        return suivantIdhistorique;
    }

    public void setSuivantIdhistorique(Historiques suivantIdhistorique) {
        this.suivantIdhistorique = suivantIdhistorique;
    }

    public Montreals getMontrealsidMontreal() {
        return montrealsidMontreal;
    }

    public void setMontrealsidMontreal(Montreals montrealsidMontreal) {
        this.montrealsidMontreal = montrealsidMontreal;
    }

    public Proprietaires getProprietaireIdproprietaire() {
        return proprietaireIdproprietaire;
    }

    public void setProprietaireIdproprietaire(Proprietaires proprietaireIdproprietaire) {
        this.proprietaireIdproprietaire = proprietaireIdproprietaire;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idhistorique != null ? idhistorique.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Historiques)) {
            return false;
        }
        Historiques other = (Historiques) object;
        if ((this.idhistorique == null && other.idhistorique != null) || (this.idhistorique != null && !this.idhistorique.equals(other.idhistorique))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Historiques[ idhistorique=" + idhistorique + " ]";
    }
    
}
