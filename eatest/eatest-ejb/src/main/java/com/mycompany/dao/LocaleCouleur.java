/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "localeCouleur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocaleCouleur.findAll", query = "SELECT l FROM LocaleCouleur l"),
    @NamedQuery(name = "LocaleCouleur.findByIdlocaleCouleur", query = "SELECT l FROM LocaleCouleur l WHERE l.idlocaleCouleur = :idlocaleCouleur"),
    @NamedQuery(name = "LocaleCouleur.findByLocale", query = "SELECT l FROM LocaleCouleur l WHERE l.locale = :locale"),
    @NamedQuery(name = "LocaleCouleur.findByDescription", query = "SELECT l FROM LocaleCouleur l WHERE l.description = :description"),
    @NamedQuery(name = "LocaleCouleur.findByCodeCouleur", query = "SELECT l FROM LocaleCouleur l WHERE l.codeCouleur = :codeCouleur")})
public class LocaleCouleur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlocaleCouleur")
    private Integer idlocaleCouleur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "locale")
    private String locale;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @Size(max = 10)
    @Column(name = "codeCouleur")
    private String codeCouleur;
    @JoinColumn(name = "couleurs_idcouleur", referencedColumnName = "idcouleur")
    @ManyToOne(optional = false)
    private Couleurs couleursIdcouleur;

    public LocaleCouleur() {
    }

    public LocaleCouleur(Integer idlocaleCouleur) {
        this.idlocaleCouleur = idlocaleCouleur;
    }

    public LocaleCouleur(Integer idlocaleCouleur, String locale) {
        this.idlocaleCouleur = idlocaleCouleur;
        this.locale = locale;
    }

    public Integer getIdlocaleCouleur() {
        return idlocaleCouleur;
    }

    public void setIdlocaleCouleur(Integer idlocaleCouleur) {
        this.idlocaleCouleur = idlocaleCouleur;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCodeCouleur() {
        return codeCouleur;
    }

    public void setCodeCouleur(String codeCouleur) {
        this.codeCouleur = codeCouleur;
    }

    public Couleurs getCouleursIdcouleur() {
        return couleursIdcouleur;
    }

    public void setCouleursIdcouleur(Couleurs couleursIdcouleur) {
        this.couleursIdcouleur = couleursIdcouleur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlocaleCouleur != null ? idlocaleCouleur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocaleCouleur)) {
            return false;
        }
        LocaleCouleur other = (LocaleCouleur) object;
        if ((this.idlocaleCouleur == null && other.idlocaleCouleur != null) || (this.idlocaleCouleur != null && !this.idlocaleCouleur.equals(other.idlocaleCouleur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.LocaleCouleur[ idlocaleCouleur=" + idlocaleCouleur + " ]";
    }
    
}
