/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "attente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Attente.findAll", query = "SELECT a FROM Attente a"),
    @NamedQuery(name = "Attente.findByIdAttente", query = "SELECT a FROM Attente a WHERE a.idAttente = :idAttente"),
    @NamedQuery(name = "Attente.findByAnneeDeFabrication", query = "SELECT a FROM Attente a WHERE a.anneeDeFabrication = :anneeDeFabrication"),
    @NamedQuery(name = "Attente.findByPhoto", query = "SELECT a FROM Attente a WHERE a.photo = :photo"),
    @NamedQuery(name = "Attente.findByNoSerie", query = "SELECT a FROM Attente a WHERE a.noSerie = :noSerie"),
    @NamedQuery(name = "Attente.findByDescriptionCouleur", query = "SELECT a FROM Attente a WHERE a.descriptionCouleur = :descriptionCouleur"),
    @NamedQuery(name = "Attente.findByDescriptionInterieur", query = "SELECT a FROM Attente a WHERE a.descriptionInterieur = :descriptionInterieur")})
public class Attente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAttente")
    private Integer idAttente;
    @Column(name = "anneeDeFabrication")
    private int anneeDeFabrication;
    @Size(max = 256)
    @Column(name = "photo")
    private String photo;
    @Column(name = "noSerie")
    private Short noSerie;
    @Size(max = 45)
    @Column(name = "descriptionCouleur")
    private String descriptionCouleur;
    @Size(max = 45)
    @Column(name = "descriptionInterieur")
    private String descriptionInterieur;
    @JoinColumn(name = "status_idstatus", referencedColumnName = "idstatus")
    @ManyToOne(optional = false)
    private Status statusIdstatus;
    @JoinColumn(name = "proprietaires_idproprietaire", referencedColumnName = "idproprietaire")
    @ManyToOne(optional = false)
    
    private Proprietaires proprietairesIdproprietaire;
   
    @JoinColumn(name = "interieurs_idinterieur", referencedColumnName = "idinterieur")
    @ManyToOne(optional = false)
    private Interieurs interieursIdinterieur;
    @JoinColumn(name = "couleurs_idcouleur", referencedColumnName = "idcouleur")
    @ManyToOne(optional = false)
    private Couleurs couleursIdcouleur;

    public Attente() {
    }

    public Attente(Integer idAttente) {
        this.idAttente = idAttente;
    }

    public Integer getIdAttente() {
        return idAttente;
    }

    public void setIdAttente(Integer idAttente) {
        this.idAttente = idAttente;
    }

    public int getAnneeDeFabrication() {
        return anneeDeFabrication;
    }

    public void setAnneeDeFabrication(int anneeDeFabrication) {
        this.anneeDeFabrication = anneeDeFabrication;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Short getNoSerie() {
        return noSerie;
    }

    public void setNoSerie(Short noSerie) {
        this.noSerie = noSerie;
    }

    public String getDescriptionCouleur() {
        return descriptionCouleur;
    }

    public void setDescriptionCouleur(String descriptionCouleur) {
        this.descriptionCouleur = descriptionCouleur;
    }

    public String getDescriptionInterieur() {
        return descriptionInterieur;
    }

    public void setDescriptionInterieur(String descriptionInterieur) {
        this.descriptionInterieur = descriptionInterieur;
    }

    public Status getStatusIdstatus() {
        return statusIdstatus;
    }

    public void setStatusIdstatus(Status statusIdstatus) {
        this.statusIdstatus = statusIdstatus;
    }

    public Proprietaires getProprietairesIdproprietaire() {
        return proprietairesIdproprietaire;
    }

    public void setProprietairesIdproprietaire(Proprietaires proprietairesIdproprietaire) {
        this.proprietairesIdproprietaire = proprietairesIdproprietaire;
    }
    
    public Interieurs getInterieursIdinterieur() {
        return interieursIdinterieur;
    }

    public void setInterieursIdinterieur(Interieurs interieursIdinterieur) {
        this.interieursIdinterieur = interieursIdinterieur;
    }

    public Couleurs getCouleursIdcouleur() {
        return couleursIdcouleur;
    }

    public void setCouleursIdcouleur(Couleurs couleursIdcouleur) {
        this.couleursIdcouleur = couleursIdcouleur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAttente != null ? idAttente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attente)) {
            return false;
        }
        Attente other = (Attente) object;
        if ((this.idAttente == null && other.idAttente != null) || (this.idAttente != null && !this.idAttente.equals(other.idAttente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Attente[ idAttente=" + idAttente + " ]";
    }
    
}
