/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "temppays")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Temppays.findAll", query = "SELECT t FROM Temppays t"),
    @NamedQuery(name = "Temppays.findByIdpays", query = "SELECT t FROM Temppays t WHERE t.idpays = :idpays"),
    @NamedQuery(name = "Temppays.findByCodePays", query = "SELECT t FROM Temppays t WHERE t.codePays = :codePays"),
    @NamedQuery(name = "Temppays.findByPays", query = "SELECT t FROM Temppays t WHERE t.pays = :pays"),
    @NamedQuery(name = "Temppays.findByLocale", query = "SELECT t FROM Temppays t WHERE t.locale = :locale")})
public class Temppays implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpays")
    private Integer idpays;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "codePays")
    private String codePays;
    @Size(max = 45)
    @Column(name = "pays")
    private String pays;
    @Size(max = 2)
    @Column(name = "locale")
    private String locale;

    public Temppays() {
    }

    public Temppays(Integer idpays) {
        this.idpays = idpays;
    }

    public Temppays(Integer idpays, String codePays) {
        this.idpays = idpays;
        this.codePays = codePays;
    }

    public Integer getIdpays() {
        return idpays;
    }

    public void setIdpays(Integer idpays) {
        this.idpays = idpays;
    }

    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpays != null ? idpays.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Temppays)) {
            return false;
        }
        Temppays other = (Temppays) object;
        if ((this.idpays == null && other.idpays != null) || (this.idpays != null && !this.idpays.equals(other.idpays))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Temppays[ idpays=" + idpays + " ]";
    }
    
}
