/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "proprietaires")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proprietaires.findAll", query = "SELECT p FROM Proprietaires p"),
    @NamedQuery(name = "Proprietaires.findByIdproprietaire", query = "SELECT p FROM Proprietaires p WHERE p.idproprietaire = :idproprietaire"),
    @NamedQuery(name = "Proprietaires.findByNom", query = "SELECT p FROM Proprietaires p WHERE p.nom = :nom"),
    @NamedQuery(name = "Proprietaires.findByPrenom", query = "SELECT p FROM Proprietaires p WHERE p.prenom = :prenom"),
    @NamedQuery(name = "Proprietaires.findByMotDePasse", query = "SELECT p FROM Proprietaires p WHERE p.motDePasse = :motDePasse"),
    @NamedQuery(name = "Proprietaires.findByActif", query = "SELECT p FROM Proprietaires p WHERE p.actif = :actif"),
    @NamedQuery(name = "Proprietaires.findByAdresseMail", query = "SELECT p FROM Proprietaires p WHERE p.adresseMail = :adresseMail")})
public class Proprietaires implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idproprietaire")
    private Integer idproprietaire;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    @Size(max = 45)
    @Column(name = "prenom")
    private String prenom;
    @Size(max = 255)
    @Column(name = "motDePasse")
    private String motDePasse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actif")
    private boolean actif;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "adresseMail")
    private String adresseMail;
    @JoinColumn(name = "pays_idpays", referencedColumnName = "idpays")
    @ManyToOne(optional = false)
    private Pays paysIdpays;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proprietairesIdproprietaire")
  //   @OneToMany(mappedBy = "proprietairesIdproprietaire")
   
    private List<Attente> attenteList;
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "proprietaireIdproprietaire")
    private List<Historiques> historiquesList;

    public Proprietaires() {
    }

    public Proprietaires(Integer idproprietaire) {
        this.idproprietaire = idproprietaire;
    }

    public Proprietaires(Integer idproprietaire, boolean actif, String adresseMail) {
        this.idproprietaire = idproprietaire;
        this.actif = actif;
        this.adresseMail = adresseMail;
    }

    public Integer getIdproprietaire() {
        return idproprietaire;
    }

    public void setIdproprietaire(Integer idproprietaire) {
        this.idproprietaire = idproprietaire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public boolean getActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }

    public Pays getPaysIdpays() {
        return paysIdpays;
    }

    public void setPaysIdpays(Pays paysIdpays) {
        this.paysIdpays = paysIdpays;
    }

    @XmlTransient
    public List<Attente> getAttenteList() {
        return attenteList;
    }

    public void setAttenteList(List<Attente> attenteList) {
        this.attenteList = attenteList;
    }

    @XmlTransient
    public List<Historiques> getHistoriquesList() {
        return historiquesList;
    }

    public void setHistoriquesList(List<Historiques> historiquesList) {
        this.historiquesList = historiquesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproprietaire != null ? idproprietaire.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proprietaires)) {
            return false;
        }
        Proprietaires other = (Proprietaires) object;
        if ((this.idproprietaire == null && other.idproprietaire != null) || (this.idproprietaire != null && !this.idproprietaire.equals(other.idproprietaire))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Proprietaires[ idproprietaire=" + idproprietaire + " ]";
    }
    
}
