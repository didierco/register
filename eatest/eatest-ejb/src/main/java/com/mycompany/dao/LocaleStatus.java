/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "localestatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocaleStatus.findAll", query = "SELECT l FROM LocaleStatus l"),
    @NamedQuery(name = "LocaleStatus.findByIdlocaleStatus", query = "SELECT l FROM LocaleStatus l WHERE l.idlocaleStatus = :idlocaleStatus"),
    @NamedQuery(name = "LocaleStatus.findByLocale", query = "SELECT l FROM LocaleStatus l WHERE l.locale = :locale"),
    @NamedQuery(name = "LocaleStatus.findByDescription", query = "SELECT l FROM LocaleStatus l WHERE l.description = :description")})
public class LocaleStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlocaleStatus")
    private Integer idlocaleStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "locale")
    private String locale;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "status_idstatus", referencedColumnName = "idstatus")
    @ManyToOne(optional = false)
    private Status statusIdstatus;

    public LocaleStatus() {
    }

    public LocaleStatus(Integer idlocaleStatus) {
        this.idlocaleStatus = idlocaleStatus;
    }

    public LocaleStatus(Integer idlocaleStatus, String locale) {
        this.idlocaleStatus = idlocaleStatus;
        this.locale = locale;
    }

    public Integer getIdlocaleStatus() {
        return idlocaleStatus;
    }

    public void setIdlocaleStatus(Integer idlocaleStatus) {
        this.idlocaleStatus = idlocaleStatus;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatusIdstatus() {
        return statusIdstatus;
    }

    public void setStatusIdstatus(Status statusIdstatus) {
        this.statusIdstatus = statusIdstatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlocaleStatus != null ? idlocaleStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocaleStatus)) {
            return false;
        }
        LocaleStatus other = (LocaleStatus) object;
        if ((this.idlocaleStatus == null && other.idlocaleStatus != null) || (this.idlocaleStatus != null && !this.idlocaleStatus.equals(other.idlocaleStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.LocaleStatus[ idlocaleStatus=" + idlocaleStatus + " ]";
    }
    
}
