/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "interieurs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Interieurs.findAll", query = "SELECT i FROM Interieurs i"),
    @NamedQuery(name = "Interieurs.findByIdinterieur", query = "SELECT i FROM Interieurs i WHERE i.idinterieur = :idinterieur"),
    @NamedQuery(name = "Interieurs.findByCodeInterieur", query = "SELECT i FROM Interieurs i WHERE i.codeInterieur = :codeInterieur")})
public class Interieurs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idinterieur")
    private Integer idinterieur;
    @Size(max = 5)
    @Column(name = "codeInterieur")
    private String codeInterieur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "interieursIdinterieur")
    private List<Montreals> montrealsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "interieursIdinterieur")
    private List<Attente> attenteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "interieursIdinterieur")
    private List<LocaleInterieur> localeinterieurList;

    public Interieurs() {
    }

    public Interieurs(Integer idinterieur) {
        this.idinterieur = idinterieur;
    }

    public Integer getIdinterieur() {
        return idinterieur;
    }

    public void setIdinterieur(Integer idinterieur) {
        this.idinterieur = idinterieur;
    }

    public String getCodeInterieur() {
        return codeInterieur;
    }

    public void setCodeInterieur(String codeInterieur) {
        this.codeInterieur = codeInterieur;
    }

    @XmlTransient
    public List<Montreals> getMontrealsList() {
        return montrealsList;
    }

    public void setMontrealsList(List<Montreals> montrealsList) {
        this.montrealsList = montrealsList;
    }

    @XmlTransient
    public List<Attente> getAttenteList() {
        return attenteList;
    }

    public void setAttenteList(List<Attente> attenteList) {
        this.attenteList = attenteList;
    }

    @XmlTransient
    public List<LocaleInterieur> getLocaleinterieurList() {
        return localeinterieurList;
    }

    public void setLocaleinterieurList(List<LocaleInterieur> localeinterieurList) {
        this.localeinterieurList = localeinterieurList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinterieur != null ? idinterieur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Interieurs)) {
            return false;
        }
        Interieurs other = (Interieurs) object;
        if ((this.idinterieur == null && other.idinterieur != null) || (this.idinterieur != null && !this.idinterieur.equals(other.idinterieur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Interieurs[ idinterieur=" + idinterieur + " ]";
    }
    
}
