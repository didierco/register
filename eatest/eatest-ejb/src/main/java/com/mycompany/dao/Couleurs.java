/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Didier
 */
@Entity
@Table(name = "couleurs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Couleurs.findAll", query = "SELECT c FROM Couleurs c"),
    @NamedQuery(name = "Couleurs.findByIdcouleur", query = "SELECT c FROM Couleurs c WHERE c.idcouleur = :idcouleur"),
    @NamedQuery(name = "Couleurs.findByCodeCouleur", query = "SELECT c FROM Couleurs c WHERE c.codeCouleur = :codeCouleur")})
public class Couleurs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcouleur")
    private Integer idcouleur;
    @Size(max = 45)
    @Column(name = "codeCouleur")
    private String codeCouleur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "couleursIdcouleur")
    private List<LocaleCouleur> localecouleurList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "couleurIdcouleur")
    private List<Montreals> montrealsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "couleursIdcouleur")
    private List<Attente> attenteList;

    public Couleurs() {
    }

    public Couleurs(Integer idcouleur) {
        this.idcouleur = idcouleur;
    }

    public Integer getIdcouleur() {
        return idcouleur;
    }

    public void setIdcouleur(Integer idcouleur) {
        this.idcouleur = idcouleur;
    }

    public String getCodeCouleur() {
        return codeCouleur;
    }

    public void setCodeCouleur(String codeCouleur) {
        this.codeCouleur = codeCouleur;
    }

    @XmlTransient
    public List<LocaleCouleur> getLocalecouleurList() {
        return localecouleurList;
    }

    public void setLocalecouleurList(List<LocaleCouleur> localecouleurList) {
        this.localecouleurList = localecouleurList;
    }

    @XmlTransient
    public List<Montreals> getMontrealsList() {
        return montrealsList;
    }

    public void setMontrealsList(List<Montreals> montrealsList) {
        this.montrealsList = montrealsList;
    }

    @XmlTransient
    public List<Attente> getAttenteList() {
        return attenteList;
    }

    public void setAttenteList(List<Attente> attenteList) {
        this.attenteList = attenteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcouleur != null ? idcouleur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Couleurs)) {
            return false;
        }
        Couleurs other = (Couleurs) object;
        if ((this.idcouleur == null && other.idcouleur != null) || (this.idcouleur != null && !this.idcouleur.equals(other.idcouleur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.dao.Couleurs[ idcouleur=" + idcouleur + " ]";
    }
    
}
