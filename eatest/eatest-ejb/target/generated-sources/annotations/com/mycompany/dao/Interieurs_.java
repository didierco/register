package com.mycompany.dao;

import com.mycompany.dao.Montreals;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-13T22:24:40")
@StaticMetamodel(Interieurs.class)
public class Interieurs_ { 

    public static volatile SingularAttribute<Interieurs, String> codeInterieur;
    public static volatile SingularAttribute<Interieurs, String> interieur;
    public static volatile ListAttribute<Interieurs, Montreals> montrealsList;
    public static volatile SingularAttribute<Interieurs, Integer> idinterieur;

}