package com.mycompany.dao;

import com.mycompany.dao.Montreals;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-13T22:24:40")
@StaticMetamodel(Status.class)
public class Status_ { 

    public static volatile SingularAttribute<Status, Integer> idstatus;
    public static volatile ListAttribute<Status, Montreals> montrealsList;
    public static volatile SingularAttribute<Status, String> status;

}