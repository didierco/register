package com.mycompany.dao;

import com.mycompany.dao.Historiques;
import com.mycompany.dao.Montreals;
import com.mycompany.dao.Proprietaires;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-13T22:24:40")
@StaticMetamodel(Historiques.class)
public class Historiques_ { 

    public static volatile SingularAttribute<Historiques, Date> debut;
    public static volatile ListAttribute<Historiques, Historiques> historiquesList;
    public static volatile ListAttribute<Historiques, Montreals> montrealsList;
    public static volatile SingularAttribute<Historiques, Proprietaires> proprietaireIdproprietaire;
    public static volatile SingularAttribute<Historiques, Date> fin;
    public static volatile SingularAttribute<Historiques, Montreals> montrealsidMontreal;
    public static volatile SingularAttribute<Historiques, Integer> idhistorique;
    public static volatile SingularAttribute<Historiques, Historiques> suivantIdhistorique;

}