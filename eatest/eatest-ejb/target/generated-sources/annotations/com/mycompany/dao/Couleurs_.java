package com.mycompany.dao;

import com.mycompany.dao.Montreals;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-13T22:24:40")
@StaticMetamodel(Couleurs.class)
public class Couleurs_ { 

    public static volatile ListAttribute<Couleurs, Montreals> montrealsList;
    public static volatile SingularAttribute<Couleurs, String> codeCouleur;
    public static volatile SingularAttribute<Couleurs, Integer> idcouleur;

}