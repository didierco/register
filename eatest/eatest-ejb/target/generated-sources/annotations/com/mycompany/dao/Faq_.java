package com.mycompany.dao;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-13T22:24:40")
@StaticMetamodel(Faq.class)
public class Faq_ { 

    public static volatile SingularAttribute<Faq, Date> dateCreation;
    public static volatile SingularAttribute<Faq, String> titre;
    public static volatile SingularAttribute<Faq, String> texte;
    public static volatile SingularAttribute<Faq, String> locale;
    public static volatile SingularAttribute<Faq, Integer> idfaq;
    public static volatile SingularAttribute<Faq, String> auteur;

}