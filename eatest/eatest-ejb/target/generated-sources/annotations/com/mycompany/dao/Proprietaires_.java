package com.mycompany.dao;

import com.mycompany.dao.Historiques;
import com.mycompany.dao.Pays;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-13T22:24:40")
@StaticMetamodel(Proprietaires.class)
public class Proprietaires_ { 

    public static volatile SingularAttribute<Proprietaires, Pays> paysIdpays;
    public static volatile ListAttribute<Proprietaires, Historiques> historiquesList;
    public static volatile SingularAttribute<Proprietaires, Integer> idproprietaire;
    public static volatile SingularAttribute<Proprietaires, String> nom;
    public static volatile SingularAttribute<Proprietaires, String> prenom;

}