package com.mycompany.dao;

import com.mycompany.dao.Proprietaires;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-12T22:29:37")
@StaticMetamodel(Pays.class)
public class Pays_ { 

    public static volatile SingularAttribute<Pays, Integer> idpays;
    public static volatile ListAttribute<Pays, Proprietaires> proprietairesList;
    public static volatile SingularAttribute<Pays, String> codePays;
    public static volatile SingularAttribute<Pays, String> pays;

}