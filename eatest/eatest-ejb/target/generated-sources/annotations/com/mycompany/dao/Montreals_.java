package com.mycompany.dao;

import com.mycompany.dao.Couleurs;
import com.mycompany.dao.Historiques;
import com.mycompany.dao.Interieurs;
import com.mycompany.dao.Status;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-01-12T22:29:37")
@StaticMetamodel(Montreals.class)
public class Montreals_ { 

    public static volatile ListAttribute<Montreals, Historiques> historiquesList;
    public static volatile SingularAttribute<Montreals, Integer> idMontreal;
    public static volatile SingularAttribute<Montreals, Historiques> historiquesIdhistorique;
    public static volatile SingularAttribute<Montreals, Interieurs> interieursIdinterieur;
    public static volatile SingularAttribute<Montreals, Date> anneeDeFabrication;
    public static volatile SingularAttribute<Montreals, String> photo;
    public static volatile SingularAttribute<Montreals, Couleurs> couleurIdcouleur;
    public static volatile SingularAttribute<Montreals, Status> statusIdstatus;

}