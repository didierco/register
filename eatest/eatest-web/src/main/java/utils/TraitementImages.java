/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;

import static java.rmi.server.LogStream.log;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@SessionScoped
public class TraitementImages implements Serializable {

    @Inject
    GetParms ca;
    private StreamedContent image;
    private StreamedContent images[];
    private List<String> listeImages;

    public TraitementImages() {

    }

    public StreamedContent getImagex() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getRenderResponse()) {

            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            try {

                //ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                //String ima = getImageParam(ec);
                //Chart
                String id = context.getExternalContext().getRequestParameterMap().get("id");

                File imageFile = new File(ca.getCheminPhotos() + "\\" + id);

                image = new DefaultStreamedContent(new FileInputStream(imageFile), "image/jpeg");

            } catch (FileNotFoundException ex) {
                System.out.println("erreur: " + ex.getMessage());
            }
            return image;
        }
    }

    public List<String> getImages() {
        //     String[] l = {"monty1.jpg", "monty2.jpg","monty3.jpg",
        //       "monty4.jpg","monty5.jpg", "monty6.jpg"};
        if (listeImages != null) {
            return listeImages;
        }
        Pattern p = Pattern.compile("^photo_SN.*");
        String[] s = new File(ca.getCheminPhotos()).list();
        ArrayList<String> lI = new ArrayList<>();
        for (int i = 0; i < s.length; i++) {
            Matcher m = p.matcher(s[i]);

            if (m.matches()) {

                lI.add(s[i]);
            }

        }
        /*
         On va aléatoirement afficher les images
         */
        Date auj = new Date();
        listeImages = new ArrayList<>();
        Random rand = new Random(auj.getTime());
     
      // int[] array ;
      //  array = new Random().ints(lI.size(), 0, lI.size()).toArray();

        for (int idx = 0; idx < lI.size(); ++idx) {

            listeImages.add(lI.get(rand.nextInt(lI.size())));
        }

        return listeImages;
    }
//get value from "f:param" appelé par la vue

    private String getImageParam(ExternalContext fc) {

        Map<String, String> params = fc.getRequestParameterMap();
        return params.get("image_ID");

    }

}
