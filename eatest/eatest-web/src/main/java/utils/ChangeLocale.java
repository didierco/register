package utils;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


@SessionScoped

@Named("locale")
public class ChangeLocale implements Serializable{
  // la locale des pages
  private String locale="fr";
  
  public ChangeLocale() {
    
      locale = "fr";
        System.out.println("init localt :"+getLocale());
  }
  
  public String setFrenchLocale(){
    locale="fr";
    return null;
  }
  
  public String setEnglishLocale(){
    locale="en";
    return null;
  }

  public String getLocale() {
      
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }
  public String laLocale() {
    return locale;
  }
  
}
