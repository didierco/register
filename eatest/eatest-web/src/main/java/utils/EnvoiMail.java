/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.mycompany.dao.Attente;
import java.io.Serializable;
import java.util.Properties;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author Didier
 */
@SessionScoped
public class EnvoiMail implements Serializable {
    @Inject
    GetParms ca;
    public  Boolean envoieUnMail(String destinataire,String leTitre, String leMail)
   {    
      // Recipient's email ID needs to be mentioned.
      String to = destinataire;

      // Sender's email ID needs to be mentioned
      String from = "leRegistreMontreal@wanadoo.fr";

     
      // Get system properties
      Properties properties = System.getProperties();
	properties.put("mail.smtp.host", ca.getServeurSMTP());
        properties.setProperty("mail.user", ca.getUserSMTP());
        properties.setProperty("mail.password", ca.getPwdSMTP());
        // javax.mail.Session pour que Ã§a fonctionne aussi avec WebObjects
    	javax.mail.Session session = javax.mail.Session.getDefaultInstance(properties, null);
  
      // Setup mail server
    //  properties.setProperty("mail.smtp.host", host);

      // Get the default Session object.
      //Session session = Session.getDefaultInstance(properties);

      try{
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO,
                                  new InternetAddress(to));

         // Set Subject: header field
         message.setSubject(leTitre);

         // Now set the actual message
         message.setText(leMail);

         // Send message
         Transport.send(message);
         System.out.println("Sent message successfully....");
      }catch (MessagingException mex) {
         mex.printStackTrace();
         return false;
      }
      
      return true;
   }
    
public void confirmeParMail(Attente attente){
        // je m'envoie un mail en cas de rÃ©ussite
                    envoieUnMail("dcourtiade@wanadoo.fr","Registre Montreal,nouvel enregistrement!!",
                            "**Une Montreal vient de s'enregistrer***\n"
                            + "\n==================================="
                            + "\nPar: "
                             +       attente.getProprietairesIdproprietaire().getAdresseMail()
                            
                           
                            + "\nAnnÃ©e :"
                            + attente.getAnneeDeFabrication()
                            + "\nNo de serie :"
                            + attente.getNoSerie()
                            + "\n=====================================");
                    // jenvoie un mail au declarant
                    envoieUnMail( attente.getProprietairesIdproprietaire().getAdresseMail(),"Registre Montreal,votre demande est prise en compte!!",
                            "**Bonjour "
                                         +       attente.getProprietairesIdproprietaire().getPrenom()
                            + " ,"
                            + "\nVotre demande d'enregistrement a été prise en compte,"
                                    + " vous allez recevoir prochainement un message de confirmation par l'administrateur."
                           
                            + "\n=====================================");
                    
}    
}
