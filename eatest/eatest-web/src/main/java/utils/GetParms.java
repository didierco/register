/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.mycompany.BeanEnreg;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/**
 *
 * @author Didier
 */
@SessionScoped
public class GetParms implements Serializable {

    private String cheminPhotos;
    private String serveurSMTP;
    private String userSMTP;
    private String pwdSMTP;
    private String portSMTP;
    public GetParms() {
    }

    public String getCheminPhotos() {
        /*
         on ne fait le traitement que si
         on n'a pas deja lu le chemin
         le bean est session donc on ne doit pouvoir lire qu'une seule fois 
         */
        if (cheminPhotos == null) {
            litLesParms();
        }

        return cheminPhotos;
    }

    public String getServeurSMTP() {
        if (serveurSMTP == null) {
            litLesParms();
        }
      return serveurSMTP;
    }
 public String getPortSMTP() {
        if (portSMTP == null) {
            litLesParms();
        }
      return portSMTP;
    }

    public String getUserSMTP() {
        if (userSMTP == null) {
            litLesParms();
        }

        return userSMTP;
    }

    public String getPwdSMTP() {
        if (pwdSMTP == null) {
            litLesParms();
        }

        return pwdSMTP;
    }

    public void setCheminPhotos(String cheminPhotos) {
        this.cheminPhotos = cheminPhotos;
    }

    private void litLesParms() {
        FileInputStream in = null;

        // recupere le chemin du fichier de config contenant le chemin vers
        // le répertoire de stockage des photos
        ServletContext servtxt = (ServletContext) FacesContext
                .getCurrentInstance().getExternalContext().getContext();
        String fichier = servtxt.getInitParameter("config");
        Properties prop = new Properties();

        /* Ici le fichier contenant les données de configuration est nommé 'db.myproperties' */
        try {
            //test pour savoir ou on pointe avec le nom de fichier passé en parm
            // java.io.File fic = new java.io.File(fichier);
            //fic.createNewFile(); // Cette fonction doit être appelée au sein d'un bloc TRY
            in = new FileInputStream(fichier);
            prop.load(in);
            in.close();
            // Extraction des propriétés
            if ((cheminPhotos = prop.getProperty("chemin")) == null) {
                System.out.println("le parametre 'chemin' n'est pas défini dans le fichier de config "
                        + fichier);
            }
            if ((serveurSMTP = prop.getProperty("serveurSMTP")) == null) {
                System.out.println("le parametre 'serveurSMTP' n'est pas défini dans le fichier de config "
                        + fichier);
            }
            if ((userSMTP = prop.getProperty("userSMTP")) == null) {
                System.out.println("le parametre 'userSMTP' n'est pas défini dans le fichier de config "
                        + fichier);
            }
            if ((pwdSMTP = prop.getProperty("pwdSMTP")) == null) {
                System.out.println("le parametre 'pwdSMTP' n'est pas défini dans le fichier de config "
                        + fichier);
            }
            if ((pwdSMTP = prop.getProperty("portSMTP")) == null) {
                System.out.println("le parametre 'portSMTP' n'est pas défini dans le fichier de config "
                        + fichier);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BeanEnreg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BeanEnreg.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(BeanEnreg.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
