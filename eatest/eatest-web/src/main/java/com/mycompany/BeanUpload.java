/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Didier
 */
@SessionScoped
@Named(value = "upload")
public class BeanUpload implements Serializable {
    
  
    private StreamedContent image;
    private String nomdeLImage;
    private byte[] bytes; // No getter+setter!
    private StreamedContent filePreview; // Getter only.
    private boolean uneImage;   // permet de savoir si une image est chargée

    /**
     * Creates a new instance of BeanUpload
     */
    public BeanUpload() {
        setUneImage(false);
    }
    
    public StreamedContent getImage() {
        System.out.println("upload l'image");
        return image;
    }
    
    public void setImage(StreamedContent image) {
        this.image = image;
    }
    
    public void handleFileUpload(FileUploadEvent event) throws IOException {
        
        InputStream input = event.getFile().getInputstream();
        
        bytes = new byte[input.available()];
        try {
            int toto = IOUtils.read(input, bytes);

            //    IOUtils.read(input, bytes);
        } finally {
            IOUtils.closeQuietly(input);
        }
        
        image = new DefaultStreamedContent(new ByteArrayInputStream(bytes), "image/jpeg");
        setUneImage(true);
    }
    
    public Boolean sauveImage(String fileName) {
        if (isUneImage() == true) {
            try {
                OutputStream output = null;

                // recupere le numero de session pour creer le nom du fichier
                FacesContext fCtx = FacesContext.getCurrentInstance();
                HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
                String sessionId = session.getId();
                System.out.println("file name:" + fileName + " id session:" + sessionId);
                output = new FileOutputStream(new File(fileName));
                IOUtils.write(bytes, output);
                
                IOUtils.closeQuietly(output);
                return false;
            } catch (IOException ex) {
                // erreur de sauvegarde, on detruit l'image sauvegardée
                Logger.getLogger(BeanUpload.class.getName()).log(Level.SEVERE, null, ex);
                
                return true;
            }
        }
        System.out.println("pas d'image");
        return false;
    }
    
    public void reset() {
        System.out.println("Reset");
        setUneImage(false);
    }

    public boolean isUneImage() {
        return uneImage;
    }
    
    public void setUneImage(boolean uneImage) {
        this.uneImage = uneImage;
    }
}
