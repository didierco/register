 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.dao.Attente;
import com.mycompany.dao.Couleurs;
import com.mycompany.dao.Interieurs;
import com.mycompany.dao.LocaleCouleur;
import com.mycompany.dao.LocaleInterieur;
import com.mycompany.dao.LocalePays;
import com.mycompany.dao.LocaleStatus;
import com.mycompany.dao.Proprietaires;
import com.mycompany.dao.Status;

import com.mycompany.metier.Erreur;
import com.mycompany.metier.Erreurs;
import com.mycompany.metier.Metier;
import com.mycompany.metiers.AttenteFacadeLocal;
import com.mycompany.metiers.LocalecouleurFacadeLocal;
import com.mycompany.metiers.LocaleinterieurFacadeLocal;
import com.mycompany.metiers.LocalepaysFacadeLocal;
import com.mycompany.metiers.LocalestatusFacadeLocal;
import com.mycompany.metiers.ProprietairesFacadeLocal;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import utils.ChangeLocale;
import utils.EnvoiMail;
import utils.GetParms;

/**
 *
 * @author Didier
 */
@SessionScoped
@Named("enreg")
public class BeanEnreg implements Serializable {

    // bean Application
    //  public void setBundle(ResourceBundle bundle) {
    //    this.bundle = bundle;
    //}
    //@Inject
//@ManagedProperty("#{msg}")
//private ResourceBundle bundle; // +setter
    Boolean accueilRendered = true;
    Boolean registreRendered = false;
    Boolean faqRendered = false;
    Boolean enregRendered = false;
    Boolean adminRendered = false;
    Boolean erreurRendered = false;
    private int noSerie;

    private String drive = "L";
    private int couleurChoisie;
    private int interieurChoisi;
    private int statusChoisi;
    private int paysChoisi;

    private String descCouleur;
    private boolean renderDesc = true;
    private List<LocaleCouleur> lesCouleurs; // va contenir ma liste des couleurs dispo
    private List<LocaleStatus> lesStatus; // va contenir ma liste des couleurs dispo
    private List<LocaleInterieur> lesInterieurs; // va contenir ma liste des interieurs
    private List<LocalePays> lesPays; // va contenir ma liste des pays
    private int idInterieur; // id de l'interieur choisi (dans la table)
    private int idCouleur;   // id de la couleur choisie (dans la table)
    private int idPays;   // id du pays choisi (dans la table)
    private int anneeChoisie;
    private int numDeSerie;
    private String nom;
    private String prenom;
    private String adresseMail;
    private String password;
    private   Attente attente;
    @EJB
    private Erreurs erreurs;
    @EJB
    private Metier metier;
    @EJB
    private LocalecouleurFacadeLocal couleurMetier;
    @EJB
    private LocaleinterieurFacadeLocal interieurMetier;
    @EJB
    private ProprietairesFacadeLocal proprioMetier;
    @EJB
    private AttenteFacadeLocal attenteMetier;
    @EJB
    private LocalestatusFacadeLocal statusMetier;
    @EJB
    private LocalepaysFacadeLocal paysMetier;
    // on injecte le bean de connnexion qui contient le propriÃ©taire connectÃ©
    @Inject
    BeanLogin userB;
    @Inject
    GetParms ca;
    @Inject
    EnvoiMail mail;
    /*
     injecte le bean gerant les locales 
     */
    //  @ManagedProperty(value = "#{locale}")
    @Inject
    private ChangeLocale changeLocale;

    public void setChangeLocale(ChangeLocale changeLocale) {

        this.changeLocale = changeLocale;
    }
    @Inject
    private BeanUpload upload;

    public void setBeanUpload(BeanUpload upload) {

        this.upload = upload;
    }
    @Inject
    private BeanForm forme;

    public void setBeanForm(BeanForm forme) {

        this.forme = forme;
    }

    public ChangeLocale getChangeLocale() {
        return this.changeLocale;
    }

    @PostConstruct
    private void init() {
        // va chercher le répertoire dans lequel sont stockées
        // les photos

        //init de la conduite
        setDrive("R");
        /*
         init de la liste des couleurs dispo
            
         */
        lesCouleurs = couleurMetier.findAll();
        lesInterieurs = interieurMetier.findAll();
        lesStatus = statusMetier.findAll();
        lesPays = paysMetier.findAll();
        System.out.println("Init() lesPays:" + lesPays.size());
        setRenderDesc(true);

    }

    /*
     Le bouton d'envoi de la page d'enregistrement a Ã©tÃ© cliquÃ©..
     */
    public void saveMonty() {
        Locale locale = new Locale(changeLocale.getLocale());
        System.out.println("save le proprio:" + userB.getCurrentUser());

        if (!(metier.getErreur())) {// erreur dans les validation...
            // validations logiques
            if ((validSaisie())) { // on envoie de message d'enregistrement

                boolean rc = persiste();
                if (rc) {
                    // je m'envoid un mail en cas de réussite
                    mail.confirmeParMail(attente);
                   
                    erreurs.add(new Erreur("W", "recordOK"));

                    forme.setForms(true, false, false, false, false, false);
                    if (!userB.isLoggedIn()) // si l'utilisateur n'est pas connexté on remet les donénes proprietaire à null
                    {
                        userB.setLeProprio();
                    }
                } else {
                    erreurs.add(new Erreur("E", "recordNOK"));
                }
                metier.setErreur(false); // reset du code erreur
                erreurs.sendErreurs(locale);
                erreurs.reset();
            }
        }
        if (metier.getErreur()) {

            metier.setErreur(false); // reset du code erreur
            erreurs.sendErreurs(locale);

            erreurs.reset();
        }
        //  FacesContext.getCurrentInstance().addMessage(null,
        //          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", toto));
    }

    /*
     Valide la saisie des champs
     */
    private boolean validSaisie() {
        // numero de serie dans la base des numero

        // la voiture est deja en base...
        if (metier.isExist(noSerie)) {
            erreurs.add(new Erreur("E", "dejaRegistre"));
        }

        // va sauvegarder la photo de la voiture avec comme nom de fichier 
        // le numero de sÃ©rie de la voiture
        if (upload.sauveImage(ca.getCheminPhotos() + "Photo_SN" + noSerie)) {
            erreurs.add(new Erreur("E", "enregFichier"));
        }
        if (!(metier.isSerieExist(noSerie, getAnnee(), getDrive().charAt(0)))) {

            erreurs.add(new Erreur("W", "noSerieBase"));

        }
        // Controle des 2 paswwords
        if (userB.ctrlPwd()) {
            erreurs.add(new Erreur("E", "badPwd"));
        }
        if (userB.isExistProprio()) {
            erreurs.add(new Erreur("E", "proprioExist"));
        }

        // Controle de infos obligatoires
        if (userB.ctrlInfos()) {
            erreurs.add(new Erreur("E", "badProprio"));
        }
        // erreur 
        if (erreurs.isUneErreur()) {
            metier.setErreur(true);
            upload.setUneImage(false); // si erreur ici on reset la photo
            return false;
        } else {
            return true;
        }
    }

    /*
     Appelle la fonction metier de persistance 
     de la table attente
     */
    private boolean persiste() {
        Proprietaires leProprio = userB.getLeProprio();

        /* l'entité a été créé on la persiste */
        if (leProprio.getIdproprietaire() == null) {
            proprioMetier.create(leProprio);
        }

        System.out.println("***login: id:" + leProprio.getIdproprietaire() + " " + leProprio.getNom() + " " + leProprio.getAdresseMail() + " " + leProprio.getPrenom());
        Couleurs coul = new Couleurs();
        coul.setIdcouleur(lesCouleurs.get(couleurChoisie).getCouleursIdcouleur().getIdcouleur());

        Interieurs intr = new Interieurs();
        intr.setIdinterieur(lesInterieurs.get(interieurChoisi).getInterieursIdinterieur().getIdinterieur());

        Status stat = new Status();
        stat.setIdstatus(lesStatus.get(statusChoisi).getStatusIdstatus().getIdstatus());
        attente = new Attente();
        attente.setProprietairesIdproprietaire(leProprio);

        attente.setAnneeDeFabrication(anneeChoisie);
        attente.setCouleursIdcouleur(coul);

        attente.setInterieursIdinterieur(intr);
        attente.setPhoto((ca.getCheminPhotos() + "SN" + noSerie));
        attente.setNoSerie((short) noSerie);
        attente.setStatusIdstatus(stat);
        //       System.out.println("liste attente avant:"+ leProprio.getAttenteList().size());
        // leProprio.getAttenteList().add(attente);
        attente.setProprietairesIdproprietaire(leProprio);
//        System.out.println("liste attente apres:"+ leProprio.getAttenteList().size());
        Boolean rc = attenteMetier.create(attente);

        return rc;

    }
    /*============================================
     Getter et Setter
     ===============================================
     */

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPaysChoisi() {

        return paysChoisi;
    }

    public void setPaysChoisi(int paysChoisi) {
        this.paysChoisi = paysChoisi;
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }

    public List<LocalePays> getLesPays() {
        // filtre sur la locale en cours
        LocalePays temp = new LocalePays();
        List<LocalePays> lx = new ArrayList<>();
        for (int j = 0; j < lesPays.size(); j++) {
            temp = lesPays.get(j);

            if (temp.getLocale().equals(changeLocale.getLocale())) {
                temp.setIdlocalePays(j);
                lx.add(temp);

            }
        }
        return lx;
    }

    public void setLesPays(List<LocalePays> lesPays) {
        this.lesPays = lesPays;
    }

    public int getAnnee() {
        return anneeChoisie;
    }
    /*
     Positionne et controle l'annÃ©e de la voiture
     annÃ©e = 0 pour les conduite Ã  droite = annÃ©e inconnue
     */

    public void setAnnee(int an) {

        if (((an == 0) || ((an >= 71) && (an <= 77)))) {

            anneeChoisie = an;
        } else {
            erreurs.add(new Erreur("E", "annee"));
            metier.setErreur(true);
        }

    }

    public String getDrive() {

        return drive;
    }

    public void setDrive(String drive) {

        if (!((drive.equals("R")) || (drive.equals("L")))) {
            erreurs.add(new Erreur("E", "drive"));
            metier.setErreur(true);
        }
        this.drive = drive;
    }

    public int getNoSerie() {

        return noSerie;
    }

    public void setNoSerie(int noSerie) {

        this.noSerie = noSerie;

    }

    /*
     Nom du proprio
     */
    public String getNom() {

        return nom;
    }

    public void setNom(String nom) {

        this.nom = nom;
    }
    /*
     Prenom du proprio
     */

    public String getPrenom() {

        return prenom;
    }

    public void setPrenom(String prenom) {

        this.prenom = prenom;
    }
    /*
     Renvoi une liste de couleur en fonction de la locale utilisÃ©e
     */

    public List<LocaleCouleur> getLesCouleurs() {
        // filtre sur la locale en cours
        LocaleCouleur temp = new LocaleCouleur();
        List<LocaleCouleur> lx = new ArrayList<>();
        for (int j = 0; j < lesCouleurs.size(); j++) {
            temp = lesCouleurs.get(j);

            if (temp.getLocale().equals(changeLocale.getLocale())) {
                temp.setIdlocaleCouleur(j);
                lx.add(temp);

            }
        }
        return lx;
    }

    public void setLesCouleurs(List<LocaleCouleur> lesCouleurs) {
        this.lesCouleurs = lesCouleurs;
    }
    /*
     Renvoi une liste de couleur en fonction de la locale utilisÃ©e
     */

    public List<LocaleInterieur> getLesInterieurs() {
        // filtre sur la locale en cours
        LocaleInterieur temp = new LocaleInterieur();
        List<LocaleInterieur> lx = new ArrayList<>();
        for (int j = 0; j < lesInterieurs.size(); j++) {
            temp = lesInterieurs.get(j);

            if (temp.getLocale().equals(changeLocale.getLocale())) {
                temp.setIdlocaleInterieur(j);
                lx.add(temp);

            }
        }
        return lx;
    }

    public void setLesInterieurs(List<LocaleInterieur> lesInterieurs) {
        this.lesInterieurs = lesInterieurs;
    }

    public List<LocaleStatus> getLesStatus() {
        // filtre sur la locale en cours
        LocaleStatus temp = new LocaleStatus();
        List<LocaleStatus> lx = new ArrayList<>();
        for (int j = 0; j < lesStatus.size(); j++) {
            temp = lesStatus.get(j);

            if (temp.getLocale().equals(changeLocale.getLocale())) {
                temp.setIdlocaleStatus(j);
                lx.add(temp);

            }
        }
        return lx;
    }

    public void setLesStatus(List<LocaleStatus> lesStatus) {
        this.lesStatus = lesStatus;
    }

    /*
     Evenement lancÃ© si l'utilisateur choisi une valeur
     dans la liste box des clouleurs
     on regarde s'il a choisi 'CUSTOM' et Ã  ce moment on affiche 
     le champs de saisie de la description 
     */
//public void couleurChanged(ValueChangeEvent e) {
    //assign new value to localeCode
    //
    public void couleurChanged() {
        //
//         String code = e.getNewValue().toString();
        int code = couleurChoisie;
        System.out.println("Hi couleur changed :" + code);

        System.out.println("couleur changÃ©e:" + lesCouleurs.get(code).getCodeCouleur());
        if (lesCouleurs.get(code).getCodeCouleur().equals("CUSTOM")) {

            // lance le javascript d'affichage du champs description
            // si on o a pris CUSTOM
            setRenderDesc(false);
            RequestContext.getCurrentInstance().execute("enableCouleur()");

        } else {
            // sinon on grise le champ
            setRenderDesc(true);
            RequestContext.getCurrentInstance().execute("disableCouleur()");
        }
    }

    public int getCouleurChoisie() {
        return couleurChoisie;
    }

    public void setCouleurChoisie(int couleurChoisie) {
        System.out.println("setCouleurChoisie :" + couleurChoisie);
        if (lesCouleurs.size() >= couleurChoisie) { // l'index doit etre maxi = au nombre d'Ã©lÃ©ment dans la liste
            this.couleurChoisie = couleurChoisie;
            System.out.println("couleur:" + this.couleurChoisie);

        } else {
            erreurs.add(new Erreur("E", "couleur"));
            metier.setErreur(true);
        }
    }

    public int getInterieurChoisi() {
        return interieurChoisi;
    }

    public void setInterieurChoisi(int interieurChoisi) {
        System.out.println("setInterieurChoisi :" + interieurChoisi);

        if (lesInterieurs.size() >= interieurChoisi) { // l'index doit etre maxi = au nombre d'Ã©lÃ©ment dans la liste
            this.interieurChoisi = interieurChoisi;
            System.out.println("Interieur:" + this.interieurChoisi);

        } else {
            erreurs.add(new Erreur("E", "interieur"));
            metier.setErreur(true);
        }
    }

    public int getStatusChoisi() {
        return statusChoisi;
    }

    public void setStatusChoisi(int statusChoisi) {
        System.out.println("setStatusChoisi :" + statusChoisi);

        if (lesStatus.size() >= statusChoisi) { // l'index doit etre maxi = au nombre d'Ã©lÃ©ment dans la liste
            this.statusChoisi = statusChoisi;
            System.out.println("Status:" + this.statusChoisi);

        } else {
            erreurs.add(new Erreur("E", "status"));
            metier.setErreur(true);
        }
    }

    public String getDescCouleur() {
        return descCouleur;
    }

    public void setDescCouleur(String descCouleur) {
        this.descCouleur = descCouleur;
    }

    public boolean getRenderDesc() {
        return renderDesc;
    }

    public void setRenderDesc(boolean renderDesc) {
        this.renderDesc = renderDesc;
    }

}
