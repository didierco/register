/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.dao.Faq;
import com.mycompany.dao.LocaleCouleur;
import com.mycompany.dao.News;
import com.mycompany.metier.Metier;

import com.mycompany.metier.Erreurs;
import com.mycompany.metiers.FaqFacadeLocal;
import com.mycompany.metiers.NewsFacadeLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import utils.ChangeLocale;

/**
 *
 * @author Didier
 */
@SessionScoped
@Named("form")
public class BeanForm implements Serializable {

    // bean Application
    //  public void setBundle(ResourceBundle bundle) {
    //    this.bundle = bundle;
    //}
    //@Inject
//@ManagedProperty("#{msg}")
//private ResourceBundle bundle; // +setter
    Boolean accueilRendered = true;
    Boolean registreRendered = false;
    Boolean faqRendered = false;
    Boolean enregRendered = false;
    Boolean adminRendered = false;
    Boolean erreurRendered = false;
    private String noSerie;
    private String annee;
    private String drive = "L";
    private String couleurChoisie;
    private String descCouleur;
    private boolean renderDesc = true;
    private List<LocaleCouleur> lesCouleurs; // va contenir ma liste des couleurs dispo
    private List<News> lesNews; // va contenir ma liste des couleurs dispo     

    Erreurs erreurs = new Erreurs();
    @EJB
    private Metier metier;
    @EJB
    private FaqFacadeLocal faqMetier;
    @EJB
    private NewsFacadeLocal newsMetier;
    /*
     injecte le bean gerant les locales 
     */
    //  @ManagedProperty(value = "#{locale}")
    @Inject
    private ChangeLocale changeLocale;

    public void setChangeLocale(ChangeLocale changeLocale) {

        this.changeLocale = changeLocale;
    }

    public ChangeLocale getChangeLocale() {
        return this.changeLocale;
    }

    @PostConstruct
    private void init() {
   // lecture des news qui sont affichées sur la premiere page...

        lesNews = reformatNews();
        
        System.out.println("cest linit :" + lesNews.size());
   
    }

// affichage vue
    public void setForms(Boolean accueilRendered, Boolean registreRendered, Boolean faqRendered,
            Boolean enregRendered, Boolean adminRendered, Boolean erreurRendered) {
        this.accueilRendered = accueilRendered;
        this.registreRendered = registreRendered;
        this.faqRendered = faqRendered;
        this.enregRendered = enregRendered;
        this.adminRendered = adminRendered;
        this.erreurRendered = erreurRendered;
    }

    // liste des FAQS
    public List<FaqList> getFaqs() {

        List<Faq> findAll;
        String locale = changeLocale.getLocale();

        findAll = faqMetier.findAll();

        List<FaqList> laList;
// Localize la liste
        laList = reformFaq(findAll, locale);

        return laList;

    }
    
    /*
     liste des news affichées sur le cote de l'ecran d'accueil
     On appelle find all pour retrier en descendant
    */
    public List<News> getAllNews() {


        return lesNews;

    }
  
    /*
     renvoi le titre et le texte de la une, news #0
    Se retouve à la fin de la liste 
    qui est triée en Descendant
    au moment de la lecture de la base
     */

    public String getUneTitre() {

        return lesNews.get(lesNews.size()-1).getTitre();
    }
    

    public String getUneTexte() {

        return lesNews.get(lesNews.size()-1).getTexte();
    }
     /*
     Lit la base de donnée  avec un tri descendant 
    afin d'avoir les dernieres en premier...

     */
    private List<News> reformatNews() {
        List<News> news = newsMetier.findAll();
        
        return news;

    }

    /*
     Reformate la list rendu par le metier en array lisible par la datatable

     */
    private List<FaqList> reformFaq(List<Faq> faq, String locale) {
        List<FaqList> result;
        result = new ArrayList<>();
        for (Faq faq1 : faq) {
            if (faq1.getLocale().compareTo(locale) == 0) {
                FaqList fl = new FaqList(faq1.getIdfaq(), faq1.getAuteur(), faq1.getTitre(), faq1.getTexte());

                result.add(fl);
            }
        }
        return result;

    }

    public Boolean getAccueilRendered() {
        return accueilRendered;
    }

    public Boolean getRegistreRendered() {
        return registreRendered;
    }

    public Boolean getFaqRendered() {
        return faqRendered;
    }

    public Boolean getEnregRendered() {
        return enregRendered;
    }

    public Boolean getAdminRendered() {
        return adminRendered;
    }

    public Boolean getErreurRendered() {
        return erreurRendered;
    }

    public Erreurs getErreurs() {
        return erreurs;
    }

    /*
     * Retour sur la page d'Accueil
     */
    public void doAccueil() {
        setForms(true, false, false, false, false, false);
    }

    /*
     * La page de la FAQ
     */
    public void doFaq() {

        setForms(false, false, true, false, false, false);

    }

    /*
     * La page d'enregistrement d'une voiture
     */
    public void doEnregistrer() {
        setForms(false, false, false, true, false, false);
    }
    /*
     * La page d'acces à l'admin
     */

    public String doAdmin() {
        return "admin";

    }
    /*
     * La page d'affichage du registre
     */

    public void doRegistre() {
        setForms(false, true, false, false, false, false);
    }

}
