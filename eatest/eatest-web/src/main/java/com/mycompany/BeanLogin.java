/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.dao.Proprietaires;
import com.mycompany.metier.Erreur;
import com.mycompany.metier.Erreurs;
import com.mycompany.metiers.ProprietairesFacadeLocal;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import utils.ChangeLocale;

/**
 *
 * @author Didier
 */
@Named(value = "login")
@SessionScoped
public class BeanLogin implements Serializable {

    @EJB
    private Erreurs erreurs;

    @EJB
    private ProprietairesFacadeLocal proprio;
    @Inject
    Credentials credentials;
    @Inject
    private ChangeLocale changeLocale;
    @Inject
    private BeanEnreg enreg;

    //private User user;
    private String user;
    /* on va avoir le propriétaire qui s'est connecté
     // on va s'en servir dans enreg...
     // ou bien on va le construire avec les infos saisies à l'ecran
     */
    private Proprietaires leProprio;

    public void login() {
        user = null;
        System.out.println("login locale:");
        Locale locale = new Locale(changeLocale.getLocale());
        System.out.println("login locale:" + changeLocale.getLocale());
        RequestContext.getCurrentInstance().update("formulaire:currentLog");

        leProprio = proprio.findProprio(credentials.getAdresseMail(), credentials.getPassword());

        /* par defaut on active le pavé de saisie des info propriétaire
         // n'est utile que quand on est sur l'ecran d'enregistrement...
         On doit pouvoir tester sur quel écran on se trouve */
        RequestContext.getCurrentInstance().execute("enableProprio()");
        if (leProprio != null) {

            if (leProprio.getActif() == true) {
                user = leProprio.getPrenom() + " " + leProprio.getNom();
                /*
                 ferme la fenetre de login
                 Affiche le nom de l'utilisateur
                 eteint le bouton login
                 */
                cleanLogin();
                RequestContext.getCurrentInstance().execute("disableProprio()");

            } else {
                erreurs.add(new Erreur("E", "NotActif"));
                erreurs.sendErreurs(locale);
                erreurs.reset();
                //     FacesMessage erreurMsg = new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Inactif");
                //   FacesContext.getCurrentInstance().addMessage(null, erreurMsg);
                RequestContext.getCurrentInstance().update("loginF"); // affiche le message

            }
        } else {
            erreurs.add(new Erreur("E", "Unknown"));
            erreurs.sendErreurs(locale);
            erreurs.reset();
            //   FacesMessage erreurMsg = new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Unknown");
            //  FacesContext.getCurrentInstance().addMessage(null, erreurMsg);
            RequestContext.getCurrentInstance().update("loginF"); // affiche le message
        }

    }

    public void logout() {
        System.out.println("logout");
        this.user = null;
        this.leProprio = null;
        cleanLogin();
        RequestContext.getCurrentInstance().execute("enableProprio()");
    }
    /*
     Reaffiche le nom de l'utilisateur
     ferme la fenetre et eteint la lumiére en sortant!!
     cache le panneau de saisie des infos du propriétaire,
     il existe déja
     */

    private void cleanLogin() {

        RequestContext.getCurrentInstance().update("loginF");
        RequestContext.getCurrentInstance().execute("fermeLogin()");
        RequestContext.getCurrentInstance().update("formulaire:currentLog");

    }

    public boolean isLoggedIn() {

        return user != null;

    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    String getCurrentUser() {
        System.out.println("getCurrentUser" + user);
        return user;

    }
    /*
     va renvoyé l'instance deja rempli par la fenetre de login
     si l'utilisateur s'est connecté
     ou bien avec les info remplies par l'utilisateur
     */

    public Proprietaires getLeProprio() {
        // si l'instance est deja rempli par le login on la passe  
        // sinon on construit avec les champs de siasis
        if (leProprio == null) {

            leProprio = new Proprietaires();
            leProprio.setActif(false);
            leProprio.setAdresseMail(credentials.getAdresseMail());
            leProprio.setMotDePasse(credentials.getPassword());
            leProprio.setNom(credentials.getNom());
            leProprio.setPrenom(credentials.getPrenom());
            leProprio.setPaysIdpays(enreg.getLesPays().get(credentials.getPaysChoisi()).getPaysIdpays());
            credentials.setAdresseMail("");
            credentials.setPassword("");
        }
        
        return leProprio;
    }

    public void setLeProprio() {
        this.leProprio = null;
    }
    /*
    Si l'utilisateur n'est pas connecté
     Controle d'equivalence des 2 password saisis 
     */

    public Boolean ctrlPwd() {
   if (leProprio == null) {
             return credentials.ctrlPwd();
        } else {
            return false;
        }
      
    }
    /*
     Controle des info saisies
    
     */

    public Boolean ctrlInfos() {
        /*
         On regarde si l'utilisateur n'est pas connecté sinon 
         On regarde si l'utilisateur n'est pas deja enregistré
         */
        if (leProprio == null) {
            return credentials.ctrlInfos();
        } else {
            return false;
        }
    }
    /*
     Regarde si le proprio est deja dans la base
     avec le couple adresse email et password
    
     */

    public Boolean isExistProprio() {
        /*
         On regarde si l'utilisateur n'est pas connecté sinon 
         on regarde si pas deja enregistré
         true le mec existe
         */
        if (leProprio == null) {
            Proprietaires findProprio = proprio.findProprio(credentials.getAdresseMail(), credentials.getPassword());
            if (findProprio == null) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}
