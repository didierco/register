/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

/**
 *
 * @author Didier
 */
public class FaqList {
    
 	private Integer idFaq;
	private String auteur;
        private String titre;
	private String texte;

    public FaqList(Integer idFaq, String auteur, String titre, String texte) {
        this.idFaq = idFaq;
        this.auteur = auteur;
        this.texte = texte;
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public Integer getIdFaq() {
        return idFaq;
    }

    public void setIdFaq(Integer idFaq) {
        this.idFaq = idFaq;
    }

    
    public String gettexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }
        
}
