/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Didier
 */
@Named(value = "credentials")
@RequestScoped
public class Credentials {

    private String adresseMail;
    private String password;
    private String nom;
    private String prenom;
    private int paysChoisi;
    private String passwordc; // 2eme password saisi pour controle

   

    public String getAdresseMail() {
     
        return adresseMail;
    }

    public void setAdresseMail(String adresse) {
        this.adresseMail = adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getPaysChoisi() {
        return paysChoisi;
    }

    public void setPaysChoisi(int paysChoisi) {
        this.paysChoisi = paysChoisi;
    }

   

    public String getPassword() {
        
        return password;
    }

    public void setPassword(String password) {
        System.out.println("credentials pas"+password);
        this.password = password;
    }

    public String getPasswordc() {
        return passwordc;
    }

    public void setPasswordc(String passwordc) {
        this.passwordc = passwordc;
    }
/*
    Controle d'equivalence des 2 password saisis par l'utilisateurs
    et si pas vide
    true pas bon
    */
    public Boolean ctrlPwd(){
        if (!passwordc.isEmpty()) 
        if  (passwordc.equals(password))
            return false;
        return true;
        
    }
    /*
    Controle des info saisies
    true = pas bon
    */
    public Boolean ctrlInfos(){
        if(adresseMail.matches("^\\S+@\\S+\\.\\S+$"))
           if (!nom.isEmpty())
               if (!prenom.isEmpty())
            return false;
    return true; 
    }
}
