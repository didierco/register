/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

/**
 *
 * @author Didier
 */
public class NewsList {
    
 	private Integer idNew;
	private String auteur;
        private String titre;
	private String texte;

    public NewsList(Integer idNew, String auteur, String titre, String texte) {
        this.idNew = idNew;
        this.auteur = auteur;
        this.texte = texte;
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public Integer getIdNew() {
        return idNew;
    }

    public void setIdFaq(Integer idNew) {
        this.idNew = idNew;
    }

    
    public String gettexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }
        
}
